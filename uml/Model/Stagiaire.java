
import java.util.*;

/**
 * 
 */
public class Stagiaire extends Personne {

    /**
     * Default constructor
     */
    public Stagiaire() {
    }

    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private String photo;

    /**
     * 
     */
    private String cv;

    /**
     * 
     */
    private String lieuNaissance;

    /**
     * 
     */
    private String paysNaissance;

    /**
     * 
     */
    private String expPro;

    /**
     * 
     */
    private int dureeExpPro;

    /**
     * 
     */
    private String idPoleEmp;

    /**
     * 
     */
    private Date datePoleEmp;


    /**
     * 
     */
    private Cursus cursusVise;




    /**
     * 
     */
    private Diplome diplome;

    /**
     * 
     */
    private Financement financement;

    /**
     * 
     */
    private Entreprise entreprise;

    /**
     * 
     */
    private Provenance provenance;

    /**
     * 
     */
    private StatutProfessionnel statut;

    /**
     * 
     */
    private Set<Test> tests;

    /**
     * 
     */
    private Indemnisation indemnisation;





}