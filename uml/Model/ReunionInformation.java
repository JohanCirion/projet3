
import java.util.*;

/**
 * 
 */
public class ReunionInformation {

    /**
     * Default constructor
     */
    public ReunionInformation() {
    }

    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private Date date;


    /**
     * 
     */
    private Cursus cursus;

    /**
     * 
     */
    private Set<Stagiaire> stagiaires;

    /**
     * 
     */
    private Salle salle;

    /**
     * 
     */
    private Intervenant intervenant;

}