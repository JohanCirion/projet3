
import java.util.*;

/**
 * 
 */
public abstract class Personne {

    /**
     * Default constructor
     */
    public Personne() {
    }

    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private String nom;

    /**
     * 
     */
    private String prenom;

    /**
     * 
     */
    private Date dateNaissance;

    /**
     * 
     */
    private String adresse;

    /**
     * 
     */
    private String codePostal;

    /**
     * 
     */
    private String ville;

    /**
     * 
     */
    private String mail;

    /**
     * 
     */
    private String tel;


    /**
     * 
     */
    private Utilisateur utilisateur;

}