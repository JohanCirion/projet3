
import java.util.*;

/**
 * 
 */
public class Entretien {

    /**
     * Default constructor
     */
    public Entretien() {
    }

    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private Date date;

    /**
     * 
     */
    private String commentaire;

    /**
     * 
     */
    private Boolean acceptation;

    /**
     * 
     */
    private Stagiaire stagiaire;

    /**
     * 
     */
    private Salle salle;

    /**
     * 
     */
    private Intervenant intervenant;

}