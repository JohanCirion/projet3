/*============================================================== */
/* INSERT :  SALLE               */
/*==============================================================*/

INSERT INTO `salle` (`id`, `num`) VALUES ('1', 'Salle 1');
INSERT INTO `salle` (`id`, `num`) VALUES ('2', 'Salle 2'); 
INSERT INTO `salle` (`id`, `num`) VALUES ('3', 'Salle 3'); 
INSERT INTO `salle` (`id`, `num`) VALUES ('4', 'Salle 4'); 
INSERT INTO `salle` (`id`, `num`) VALUES ('5', 'Salle 5'); 
INSERT INTO `salle` (`id`, `num`) VALUES ('6', 'Salle 6');
INSERT INTO `salle` (`id`, `num`) VALUES ('7', 'Salle 7');



/*==============================================================*/
/* INSERT :  CURSUS               */
/*==============================================================*/
INSERT INTO `cursus` (`id`, `code`, `dateInvalid`, `dateValidation`, `dureeTheoriq`, `nom`) VALUES (NULL, 'AI', NULL, '2019-10-09 00:00:00', '120', 'Analyste Informaticien');
INSERT INTO `cursus` (`id`, `code`, `dateInvalid`, `dateValidation`, `dureeTheoriq`, `nom`) VALUES (NULL, 'PO', NULL, NULL, NULL, 'Product Owner');
INSERT INTO `cursus` (`id`, `code`, `dateInvalid`, `dateValidation`, `dureeTheoriq`, `nom`) VALUES (NULL, 'AL', NULL, NULL, NULL, 'Architecte Logiciel'); 
INSERT INTO `cursus` (`id`, `code`, `dateInvalid`, `dateValidation`, `dureeTheoriq`, `nom`) VALUES (NULL, 'FS', NULL, NULL, NULL, 'Developpeur Java Fullstack');


--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (1, NULL, 'Initiation Java');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES  (2, NULL, 'Intro JEE');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (3, NULL, 'Frameword .Net : C#');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (4, NULL, 'Communication et travail de groupe');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (5, NULL, 'Informatique et S.I : introduction');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (6, NULL, 'Initiation à l’algorithmique');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (7, NULL, 'Algorithmique avancé');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (8, NULL, 'Méthodes agiles');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (9, NULL, 'Projet 1');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (10, NULL, 'Éléments d’analyse');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (11, NULL, 'Projet 2');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (12, NULL, 'Oracle / SQL / PL-SQL');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (13, NULL, 'Internet; HTTP, intro HTML');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (14, NULL, 'Webform et Asp.net');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (15, NULL, 'Unix');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (16, NULL, 'XML/XSLT');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (17, NULL, 'UML');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (18, NULL, 'JDBC');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (19, NULL, 'Projet 3');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (20, NULL, 'Qualimetrie');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (21, NULL, 'JSP Servlet');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (22, NULL, 'Struts');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (23, NULL, 'JSF');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (24, NULL, 'Hibernate');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (25, NULL, 'RMI ELB');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (26, NULL, 'JPA');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (27, NULL, 'Webservices Java Net');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (28, NULL, 'Spring');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (29, NULL, 'Projet 2 Lancement');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (30, NULL, 'Projet 3 Lancement');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (31, NULL, 'Projet 3 Avancement');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (32, NULL, 'POO & Patterns');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (33, NULL, 'TRE');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (34, NULL, 'JavaScript');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (35, NULL, 'Initiation Framework JavaScript 2.0');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (36, NULL, 'Java FX');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (37, NULL, 'Optimisation de bases de données');
INSERT INTO `matiere` (`id`, `dateInvalid`, `nom`) VALUES (38, NULL, 'Projet 2 avancement');

--
-- Déchargement des données de la table `cursusmatiere`
--

INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (1, 1, 4, 4, 1);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (2, 1, 5, 4, 2);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (3, 1, 6, 6, 3);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (4, 1, 1, 14, 4);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (5, 1, 7, 10, 5);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (6, 1, 8, 4, 6);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (7, 1, 36, 6, 7);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (8, 1, 33, 4, 8);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (9, 1, 9, 18, 9);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (10, 1, 10, 10, 10);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (11, 1, 29, 4, 11);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (12, 1, 12, 4, 12);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (13, 1, 37, 2, 13);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (14, 1, 38, 2, 14);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (15, 1, 3, 6, 15);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (16, 1, 13, 4 , 16);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (17, 1, 14, 12, 17);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (18, 1, 11, 18, 18);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (19, 1, 15, 4, 19);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES  (20, 1, 16, 4, 20);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (21, 1, 17, 6, 21);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (22, 1, 2, 2, 22 );
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES  (23, 1, 18, 2, 23);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (24, 1, 30, 2, 24);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES  (25, 1, 20, 10, 25);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (26, 1, 21, 2, 26 );
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (27, 1, 22, 2, 27);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (28, 1, 23, 4, 28);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES  (29, 1, 24, 2, 29);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES  (30, 1, 25, 2, 30);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (31, 1, 26, 2, 31);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (32, 1, 27, 2, 32);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (33, 1, 31, 2, 33);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (34, 1, 28, 6, 34);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (35, 1, 32, 4, 35);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (36, 1, 34, 6, 36);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (37, 1, 35, 8, 37);
INSERT INTO `cursusmatiere` (`id`, `cursus_id`, `matiere_id`, `duree`, `ordre`) VALUES (38, 1, 19 ,18, 38);

/*============================================================== */
/* INSERT :  PROFIL               */
/*==============================================================*/


INSERT INTO `profil` (`id`, `nom`) VALUES (NULL, 'Stagiaire');
INSERT INTO `profil` (`id`, `nom`) VALUES (NULL, 'Intervenant');
INSERT INTO `profil` (`id`, `nom`) VALUES (NULL, 'Administratif');


/*============================================================== */
/* INSERT :  UTILISATEUR               */
/*==============================================================*/

INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (1, 'adupont', 'stagiaire', 1);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (2, 'driquet', 'stagiaire', 1);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (3, 'tducot', 'stagiaire', 1);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (4, 'gvavin', 'stagiaire', 1);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (5, 'jzanetti', 'stagiaire', 1);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (6, 'baubier', 'admin', 3);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (7, 'aelmi', 'admin', 3);
INSERT INTO `utilisateur` (`id`, `login`, `password`, `profil_id`) VALUES (8, 'bchauvet', 'intervenant', 2);


--
-- Déchargement des données de la table PERSONNE
--

INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `paysNaissance`, `photo`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES('Intervenant ', 1, '33 rue Poulet', '75018', '1978-11-15 00:00:00', 'gpapin@eql.fr', 'Papin', 'Grégory', '0000000000', 'Paris', NULL, NULL, NULL, NULL, NULL, 'Oulan-Bator', 'Mongolie', 'FakeDPBoy1.jpeg', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `paysNaissance`, `photo`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES('Intervenant', 2, '58 rue Saint Denis', '93130', '1980-03-11 00:00:00', 'yanis.garcin@eql.fr', 'Garcin', 'Yanis', '0709876543', 'Noisy-Le-Sec', NULL, NULL, NULL, NULL, NULL, 'Calcutta', 'Inde', 'fakeDPBoy2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `paysNaissance`, `photo`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES('Intervenant', 3, '19 rue du Développement', '92120', '1985-12-03 00:00:00', 'hervehureau@eql.fr', 'Hureau', 'Hervé', '0000000000', 'Montrouge', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fakeDPBoy3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `paysNaissance`, `photo`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES('Intervenant', 4, '45 rue de Lovelace', '75020', '1990-02-05 00:00:00', 'cdurand@eql.fr', 'Durand', 'Catherine', '0000000000', 'Paris', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fakeDPGirl1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `paysNaissance`, `photo`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES('Administratif', 5, '35 rue de la gestion', '75001', '1980-11-12 00:00:00', 'baubier@eql.fr', 'Aubier', 'Berylle', '0000000000', 'Paris', NULL, NULL, NULL, NULL, NULL, 'Lille', 'France', 'fakeDPGirl2.jpg', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `paysNaissance`, `photo`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES('Administratif', 6, '56 rue la lumière', '75018', '1991-12-09 00:00:00', 'elmi@eql.fr', 'Elmi', 'Anaig', '0000000000', 'Paris', NULL, NULL, NULL, NULL, NULL, 'Lyon', 'France', 'fakeDPGirl3.jpg', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `photo`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Intervenant', 15, '30 rue Lamartine', '92110', '1975-10-02 00:00:00', 'mgarreau@eql.fr', 'Garreau', 'Mathieu', '01.46.82.52.85', 'Boulogne', 'fakeDPMan3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `photo`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Intervenant', '14', '64 cours Clémenceau', '92140', '1977-05-03 00:00:00', 'jfavereau@eql.fr', 'Favereau', 'Julie', '06.78.65.12.91', 'Clamart', 'profWoman.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);





/*============================================================== */
/* INSERT :  DIPLOME               */
/*==============================================================*/

INSERT INTO `diplome` (`id`, `niveau`) VALUES (1, 'Bac+2');
INSERT INTO `diplome` (`id`, `niveau`) VALUES (2, 'Bac+3');
INSERT INTO `diplome` (`id`, `niveau`) VALUES (3, 'Bac+4');
INSERT INTO `diplome` (`id`, `niveau`) VALUES (4, 'Bac+5');
INSERT INTO `diplome` (`id`, `niveau`) VALUES (5, 'Bac+6');
INSERT INTO `diplome` (`id`, `niveau`) VALUES (6, 'Bac+7');
INSERT INTO `diplome` (`id`, `niveau`) VALUES (7, 'Bac+8');

/*============================================================== */
/* INSERT :  DISPOSITIF               */
/*==============================================================*/

INSERT INTO `dispositif` (`id`, `nom`) VALUES (1, 'Conventionnement CRIF');
INSERT INTO `dispositif` (`id`, `nom`) VALUES (2, 'POEI');
INSERT INTO `dispositif` (`id`, `nom`) VALUES (3, 'POEC');
INSERT INTO `dispositif` (`id`, `nom`) VALUES (4, 'CSP');
INSERT INTO `dispositif` (`id`, `nom`) VALUES (5, 'Contrat pro.');
INSERT INTO `dispositif` (`id`, `nom`) VALUES (6, 'VAE');


/*============================================================== */
/* INSERT :  ENTREPRISE               */
/*==============================================================*/

INSERT INTO `entreprise` (`id`, `nom`) VALUES (1, 'Henix');
INSERT INTO `entreprise` (`id`, `nom`) VALUES (2, 'S&H');
INSERT INTO `entreprise` (`id`, `nom`) VALUES (3, 'Oxiane');
INSERT INTO `entreprise` (`id`, `nom`) VALUES (4, 'TLTI');

/*============================================================== */
/* INSERT :  FINANCEMENT               */
/*==============================================================*/

INSERT INTO `financement` (`id`, `nom`) VALUES (1, 'Conseil Régional IDF');
INSERT INTO `financement` (`id`, `nom`) VALUES (2, 'CPF');
INSERT INTO `financement` (`id`, `nom`) VALUES (3, 'Faf TT');
INSERT INTO `financement` (`id`, `nom`) VALUES (4, 'Fongecif');
INSERT INTO `financement` (`id`, `nom`) VALUES (5, 'Opco');
INSERT INTO `financement` (`id`, `nom`) VALUES (6, 'Pôle Emploi');


/*============================================================== */
/* INSERT :  INDEMNISATION               */
/*==============================================================*/

INSERT INTO `indemnisation` (`id`, `nom`) VALUES (1, 'ARE');
INSERT INTO `indemnisation` (`id`, `nom`) VALUES (2, 'RSA');
INSERT INTO `indemnisation` (`id`, `nom`) VALUES (3, 'Autre');


/*============================================================== */
/* INSERT :  PROVENANCE               */
/*==============================================================*/

INSERT INTO `provenance` (`id`, `nom`) VALUES (1, 'Pôle Emploi');
INSERT INTO `provenance` (`id`, `nom`) VALUES (2, 'Internet');
INSERT INTO `provenance` (`id`, `nom`) VALUES (3, 'Connaissance');
INSERT INTO `provenance` (`id`, `nom`) VALUES (4, 'Autre');


/*============================================================== */
/* INSERT :  STATUT PROFESSIONNNEL               */
/*==============================================================*/

INSERT INTO `statutprofessionnel` (`id`, `nom`) VALUES (1, 'Demandeur d’emploi');
INSERT INTO `statutprofessionnel` (`id`, `nom`) VALUES (2, 'Salarié');


/*============================================================== */
/* INSERT :  TITRE               */
/*==============================================================*/

INSERT INTO `titre` (`id`, `titre`) VALUES (1, 'Monsieur');
INSERT INTO `titre` (`id`, `titre`) VALUES (2, 'Madame');
INSERT INTO `titre` (`id`, `titre`) VALUES (3, 'Ind.');




/*============================================================== */
/* INSERT :  PROMOTION               */
/*==============================================================*/

INSERT INTO `promotion` (`id`, `dateAnnul`, `dateDebut`, `nom`, `numero`, `cursus_id`) VALUES (1, NULL, '2019-09-02 00:00:00', 'AI 1', 1, 1);

/*============================================================== */
/* INSERT :  COURS               */
/*==============================================================*/


INSERT INTO `cours` (`id`, `dateDebut`, `matin`, `intervenant_id`, `matiere_id`, `salle_id`, `promotion_id`) VALUES ('1', '2019-09-02 00:00:00', b'0000', '3', '1', '5', '1');
INSERT INTO `cours` (`id`, `dateDebut`, `matin`, `intervenant_id`, `matiere_id`, `salle_id`, `promotion_id`) VALUES ('2', '2019-09-02 00:00:00', b'00000', '3', '1', '5', '1');
INSERT INTO `cours` (`id`, `dateDebut`, `matin`, `intervenant_id`, `matiere_id`, `salle_id`, `promotion_id`) VALUES ('3', '2019-09-03 00:00:00', b'0000', '3', '1', '5', '1');
INSERT INTO `cours` (`id`, `dateDebut`, `matin`, `intervenant_id`, `matiere_id`, `salle_id`, `promotion_id`) VALUES ('4', '2019-09-03 00:00:00', b'00000', '3', '1', '5', '1');



/*============================================================== */
/* INSERT :  PERSONNE               */
/*==============================================================*/


INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', 7, '25 rue de la Paix', '75001', '1985-10-02 00:00:00', 'adupont@gmail.com', 'Dupont', 'Arthur', '01.23.45.67.89', 'Paris', 'CVAFCEPF.pdf', '2019-01-05 00:00:00', 5, 'Architecte', '1234567G', 'Perpignan', '29', '31', 'France', 'tof.jpg', 1, 1, 1, 1, 2, 5, 1, 1, 1);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', 8, '12 rue des Bois', '75013', '1986-10-02 00:00:00', 'driquet@gmail.com', 'Riquet', 'Delphine', '06.28.82.64.46', 'Paris', 'CVAFCEPF.pdf', '2018-12-10 00:00:00', 4, 'Assistance médicale', '9874563M', 'Grenoble', '24', '27', 'France', 'tof.jpg', 2, 2, 1, 3, NULL, 1, 1, 2, 1);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', 9, '115 avenue du Général de Gaulle', '92110', '1983-11-22 00:00:00', 'tducot', 'Ducot', 'Thierry', '06.98.74.56.32', 'Boulogne-Billancourt', 'CVAFCEPF.pdf', '2019-02-18 00:00:00', 9, 'Commercial', '852465M', 'Dunkerque', '26', '29', 'France', 'tof.jpg', 1, NULL, 1, 2, 4, 5, 1, 1, 1);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', 10, '63 cours Balguerie', '94200', '1987-05-07 00:00:00', 'guilvavin@gmail.com', 'Vavin', 'Guillaume', '06.32.65.98.71', 'Ivry-sur-Seine', 'CVAFCEPF.pdf', '2018-12-03 00:00:00', 7, 'Restaurateur', '7413698D', 'Biarritz', '25', '31', 'France', 'tof.jpg', 1, NULL, 1, 1, 2, 5, 1, 2, 1);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', 11, '5 avenue Pasteur', '92120', '1988-07-21 00:00:00', 'jzanetti@gmail.com', 'Zanetti', 'Julie', '06.12.45.78.93', 'Montrouge', 'CVAFCEPF.pdf', '2019-04-29 00:00:00', 4, 'WebMaster', '8527891D', 'Marseille', '31', '28', 'France', 'tof.jpg', 2, NULL, 1, 2, 1, 5, 1, 3, 1);


INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', NULL, NULL, NULL, NULL, 'amahroug@gmail.com', 'Mahroug', 'Adber', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nophoto.jpg', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', NULL, NULL, NULL, NULL, 'adelamotte@gmail.com', 'Delamotte', 'Allan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nophoto.jpg', '1', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `personne` (`type`, `id`, `adresse`, `codePostal`, `dateNaissance`, `mail`, `nom`, `prenom`, `tel`, `ville`, `cv`, `datePoleEmp`, `dureeExpPro`, `expPro`, `idPoleEmp`, `lieuNaissance`, `noteLogique`, `noteRaisonnement`, `paysNaissance`, `photo`, `titre_id`, `utilisateur_id`, `cursusVise_id`, `diplome_id`, `entreprise_id`, `financement_id`, `indemnisation_id`, `provenance_id`, `statut_id`) VALUES ('Stagiaire', NULL, NULL, NULL, NULL, 'adecroix@gmail.com', 'Decroix', 'Alexandra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nophoto.jpg', '2', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL);





/*============================================================== */
/* INSERT :  ENTRETIEN              */
/*==============================================================*/


INSERT INTO `entretien` (`id`, `acceptation`, `commentaire`, `date`, `intervenant_id`, `salle_id`, `stagiaire_id`) VALUES (1, b'1', NULL, '2019-08-19 11:00:00', 3, 6, 7);
INSERT INTO `entretien` (`id`, `acceptation`, `commentaire`, `date`, `intervenant_id`, `salle_id`, `stagiaire_id`) VALUES (2, b'1', NULL, '2019-08-20 11:00:00', 3, 6, 8);
INSERT INTO `entretien` (`id`, `acceptation`, `commentaire`, `date`, `intervenant_id`, `salle_id`, `stagiaire_id`) VALUES (3, b'1', NULL, '2019-08-21 00:00:00', 3, 6, 9);
INSERT INTO `entretien` (`id`, `acceptation`, `commentaire`, `date`, `intervenant_id`, `salle_id`, `stagiaire_id`) VALUES (4, b'1', NULL, '2019-08-22 10:00:00', 3, 6, 10);
INSERT INTO `entretien` (`id`, `acceptation`, `commentaire`, `date`, `intervenant_id`, `salle_id`, `stagiaire_id`) VALUES (5, b'1', NULL, '2019-08-16 11:00:00', 3, 6, 11);



/*============================================================== */
/* INSERT :  STAGIAIREPROMO               */
/*==============================================================*/

INSERT INTO `stagiairepromo` (`id`, `dispositif_id`, `promotion_id`, `stagiaire_id`) VALUES (1, 2, 1, 7);
INSERT INTO `stagiairepromo` (`id`, `dispositif_id`, `promotion_id`, `stagiaire_id`) VALUES (2, 1, 1, 8);
INSERT INTO `stagiairepromo` (`id`, `dispositif_id`, `promotion_id`, `stagiaire_id`) VALUES (3, 2, 1, 9);
INSERT INTO `stagiairepromo` (`id`, `dispositif_id`, `promotion_id`, `stagiaire_id`) VALUES (4, 2, 1, 10);
INSERT INTO `stagiairepromo` (`id`, `dispositif_id`, `promotion_id`, `stagiaire_id`) VALUES (5, 2, 1, 11);


/*============================================================== */
/* INSERT :  TEST               */
/*==============================================================*/


INSERT INTO `test` (`id`, `dateTest`, `nom`, `salle_id`) VALUES (1, '2019-09-02 15:30:00', NULL, 7);

/*============================================================== */
/* INSERT :  STAGIAIRETEST               */
/*==============================================================*/

INSERT INTO `stagiairetest` (`id`, `stagiaire_id`, `test_id`) VALUES (1, 7, 1);
INSERT INTO `stagiairetest` (`id`, `stagiaire_id`, `test_id`) VALUES (2, 8, 1);
INSERT INTO `stagiairetest` (`id`, `stagiaire_id`, `test_id`) VALUES (3, 9, 1);
INSERT INTO `stagiairetest` (`id`, `stagiaire_id`, `test_id`) VALUES (4, 10, 1);
INSERT INTO `stagiairetest` (`id`, `stagiaire_id`, `test_id`) VALUES (5, 11, 1);




/*==============================================================*/
/* INSERT :  RI               */
/*==============================================================*/


INSERT INTO `reunion_information` (`id`, `date`, `cursus_id`, `intervenant_id`, `salle_id`) VALUES (NULL, '2019-10-24 10:00:00', '1', '1', '2');
INSERT INTO `reunion_information` (`id`, `date`, `cursus_id`, `intervenant_id`, `salle_id`) VALUES (NULL, '2019-10-25 10:00:00', '1', '1', '2');



/*==============================================================*/
/* INSERT :  RI-PERSONNE     */
/*==============================================================*/


INSERT INTO `reunion_information_personne` (`ReunionInformation_id`, `stagiaires_id`) VALUES (1, 7);
INSERT INTO `reunion_information_personne` (`ReunionInformation_id`, `stagiaires_id`) VALUES (1, 8);
INSERT INTO `reunion_information_personne` (`ReunionInformation_id`, `stagiaires_id`) VALUES (1, 9);
INSERT INTO `reunion_information_personne` (`ReunionInformation_id`, `stagiaires_id`) VALUES (1, 10);
INSERT INTO `reunion_information_personne` (`ReunionInformation_id`, `stagiaires_id`) VALUES (1, 11);


