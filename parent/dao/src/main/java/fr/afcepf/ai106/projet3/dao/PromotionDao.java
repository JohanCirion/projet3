package fr.afcepf.ai106.projet3.dao;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.idao.PromotionIDao;

@Remote(PromotionIDao.class)
@Stateless
public class PromotionDao extends GenericDao<Promotion> implements PromotionIDao {

	@PersistenceContext(unitName="daoProjet3PU")
	private EntityManager em;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Promotion> getAllPromotions() {
		Query query = em.createQuery("SELECT p FROM Promotion p");
		List<Promotion> promotions = query.getResultList();
		return promotions;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Stagiaire> getStagiairesbyPromo(Promotion p) {
		Query query = em.createQuery("SELECT sp.stagiaire FROM StagiairePromo sp WHERE sp.promotion = :paramPromo");
		query.setParameter("paramPromo", p);
		List<Stagiaire> stagiaires = query.getResultList();
		return stagiaires;
	}


	@Override
	public List<Cours> getAllCours(Promotion p) {
		Query query = em.createQuery("SELECT c FROM Cours c WHERE c.promotion = :paramPromo");
		query.setParameter("paramPromo", p);
		List<Cours> result = query.getResultList();
		return result;
	}


	@Override
	public Integer getNumPromo(Cursus c) {
		Integer result = 0; 
		Query query = em.createQuery("SELECT MAX(p.numero) FROM Promotion p WHERE p.cursus = :paramCursus");
		query.setParameter("paramCursus", c);
		if(query.getSingleResult() != null) {
			result = (Integer) query.getSingleResult();
		}
		return result + 1;
	}


	@Override
	public Date endDate(Promotion p) {
		Query query = em.createQuery("SELECT MAX(c.dateDebut) FROM Cours c WHERE c.promotion = :paramPromo");
		query.setParameter("paramPromo", p);
		return (Date) query.getSingleResult();
	}





	

	

	
	

}
