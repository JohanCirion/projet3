package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.idao.CoursIDao;


@Remote (CoursIDao.class)
@Stateless
public class CoursDao extends GenericDao<Cours> implements CoursIDao{

	@PersistenceContext(unitName="daoProjet3PU")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Cours> getCoursByPromoMatiere(Promotion p, Matiere m) {
		Query query = em.createQuery("SELECT c FROM Cours c WHERE c.matiere = :paramMatiere AND c.promotion = :paramPromo");
		query.setParameter("paramPromo", p);
		query.setParameter("paramMatiere", m);
		List<Cours> result = query.getResultList();	
		return result;
	}


	
}
