package fr.afcepf.ai106.projet3.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.idao.CursusIDao;


@Remote (CursusIDao.class)
@Stateless
public class CursusDao extends GenericDao<Cursus> implements CursusIDao {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	@Override
	public List<Cursus> getAll() {
		Query query = em.createQuery("SELECT c FROM Cursus c" );
        
	       List<Cursus> cursus = query.getResultList();

		return cursus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CursusMatiere> getMatieres(Cursus c) {
		List<CursusMatiere> result = new ArrayList<>();
		Query query = em.createQuery("SELECT cm FROM CursusMatiere cm WHERE cm.cursus = :paramCursus ORDER BY ordre");
		query.setParameter("paramCursus", c);
		result = query.getResultList();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getDureeCursus(Cursus c) {
			int duree = 0;
			Query query = em.createQuery("SELECT cm FROM CursusMatiere cm WHERE cm.cursus = :paramCursus");
			query.setParameter("paramCursus", c);
			List<CursusMatiere> cm = query.getResultList();
			for (CursusMatiere cursusMatiere : cm) {
				duree += cursusMatiere.getDuree();
			}
			return duree;
	}

	@Override
	public List<Cursus> getCursusValides() {
		Query query = em.createQuery("SELECT c FROM Cursus c WHERE c.dateValidation IS NOT NULL");
		List<Cursus> result = query.getResultList();
		return result;
	}

	@Override
	public List<Cursus> getInvalidCursi() {
		Query query = em.createQuery("SELECT c FROM Cursus c WHERE c.dateValidation IS NULL");
		List<Cursus> result = query.getResultList();
		return result;
	}

}
