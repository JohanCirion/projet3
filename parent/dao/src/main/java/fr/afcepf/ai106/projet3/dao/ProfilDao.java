package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Profil;
import fr.afcepf.ai106.projet3.idao.ProfilIDao;

@Remote (ProfilIDao.class)
@Stateless
public class ProfilDao extends GenericDao<Profil> implements ProfilIDao {


}
