package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Administratif;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;
import fr.afcepf.ai106.projet3.idao.UtilisateurIDao;


@Remote (UtilisateurIDao.class)
@Stateless
public class UtilisateurDao extends GenericDao<Utilisateur> implements UtilisateurIDao {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;

	
	@Override
	public Utilisateur Authenticate(String login, String password) {
		Utilisateur u = null;
		Query query = em.createQuery
				("SELECT u FROM Utilisateur u WHERE u.login = :login and u.password = :password");
		query.setParameter("login", login);
		query.setParameter("password", password);
		if(!query.getResultList().isEmpty()) {
		u = (Utilisateur) query.getSingleResult();
		}
		return u;
	}

	
	@Override
	public Stagiaire getStagiaireByUtilisateur(Utilisateur u) {
		Stagiaire s = null;
		Query query = em.createQuery
				("SELECT s FROM Stagiaire s WHERE s.utilisateur = :u");
		query.setParameter("u", u);
		List<Stagiaire> stagiaires = query.getResultList();
		if (stagiaires.size()>0) {
			s = stagiaires.get(0);
		}
		
		return s;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Stagiaire> getStagiairebyName(String name) {
		Query query = em.createQuery("SELECT s FROM Stagiaire s WHERE s.nom LIKE CONCAT('%',:name,'%')");
		query.setParameter("name", name);
		List<Stagiaire> stagiaires = query.getResultList();	
		return stagiaires;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<Intervenant> getIntervenantbyName(String name) {
		Query query = em.createQuery("SELECT i FROM Intervenant i WHERE i.nom = :name");
		query.setParameter("name", name);
		List<Intervenant> intervenants = query.getResultList();	
		return intervenants;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Administratif> getAdministratifbyName(String name) {
		Query query = em.createQuery("SELECT a FROM Administratif a WHERE a.nom = :name");
		query.setParameter("name", name);
		List<Administratif> administratifs = query.getResultList();	
		return administratifs;
	}


	@Override
	public Administratif getAdminByUtilisateur(Utilisateur u) {
		Administratif a = null;
		Query query = em.createQuery
				("SELECT a FROM Administratif a WHERE a.utilisateur = :u");
		query.setParameter("u", u);
		List<Administratif> admins = query.getResultList();
		if (admins.size()>0) {
			a = admins.get(0);
		}
		return a;
	}


	@Override
	public Intervenant getIntervByUtilisateur(Utilisateur u) {
		Intervenant i = null;
		Query query = em.createQuery
				("SELECT i FROM Intervenant i WHERE i.utilisateur = :u");
		query.setParameter("u", u);
		List<Intervenant> intervs = query.getResultList();
		if (intervs.size()>0) {
			i = intervs.get(0);
		}
		return i;
	}


}
