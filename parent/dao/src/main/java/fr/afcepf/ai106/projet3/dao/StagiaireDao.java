package fr.afcepf.ai106.projet3.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.idao.ProvenanceIDao;
import fr.afcepf.ai106.projet3.idao.ReunionInformationIDao;
import fr.afcepf.ai106.projet3.idao.StagiaireIDao;

@Remote(StagiaireIDao.class)
@Stateless
public class StagiaireDao extends GenericDao<Stagiaire> implements StagiaireIDao {

	@PersistenceContext(unitName="daoProjet3PU")
	private EntityManager em;

	ReunionInformationIDao proxyRIDao;
	
	ProvenanceIDao proxyProvenanceDao;


	@Override
	@SuppressWarnings("unchecked")
	public List<Stagiaire> getAllStagiaires() {
		Query query = em.createQuery("SELECT s FROM Stagiaire s");
		List<Stagiaire> stagiaires = query.getResultList();		
		return stagiaires;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Stagiaire> rechercherUnStagiaire(String name) {
		Query query = em.createQuery("SELECT s FROM Stagiaire s WHERE s.nom LIKE CONCAT('%',:name,'%') OR s.prenom LIKE CONCAT('%',:name,'%')");
		
		query.setParameter("name", name);
		List<Stagiaire> stagiaires = query.getResultList();	
		//		for (Stagiaire stagiaire : stagiaires) {
		//			System.out.println(stagiaire.getNom());			
		//		}
		return stagiaires;
	}


	public Stagiaire getStagiaireById(int id) {
		Query query = em.createQuery("SELECT s FROM Stagiaire s WHERE s.id = :id");
		query.setParameter("id", id);
		Stagiaire s = (Stagiaire) query.getSingleResult();
		return s;		
	}
	
	


	@SuppressWarnings("unchecked")
	@Override
	public List<Stagiaire> getAllCandidats() {
		List<Stagiaire> candidats = new ArrayList<Stagiaire>();
		Query query = em.createNativeQuery("SELECT p.id FROM personne p WHERE NOT EXISTS (select null FROM reunion_information_personne r WHERE p.id = r.stagiaires_id) AND p.type = 'Stagiaire'");
		List<Integer> ids = query.getResultList();
		for (int id : ids) {
			Stagiaire s = (Stagiaire) getStagiaireById(id);			
			candidats.add(s);
		}
			
		return candidats;
	}
	
	
	public Provenance demoProvenance() {
		Provenance prov = new Provenance();
		prov = proxyProvenanceDao.rechercher(2);
		return prov;
		
	}

}
