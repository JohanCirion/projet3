package fr.afcepf.ai106.projet3.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.idao.GenericIDao;

public abstract class GenericDao<T> implements GenericIDao<T>  {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;  
    
    @Override
    public T ajouter(T t) {
        em.persist(t);
        return t;
    }

    @Override
    public boolean supprimer(T t) {

        boolean retour;
        try {
            t = em.merge(t);
            em.remove(t);
            retour = true;
        }catch (Exception e){
            e.printStackTrace();
            retour = false;
        }          
        return retour;
    }

    @Override
    public T modifier(T t) {
        em.merge(t);
        return t;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T rechercher(Integer i) {
        T t = null;
        try {
        String className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
        Class<?> clazz;  
            clazz = Class.forName(className);
            t = (T) em.find(clazz, i);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return t;
    }
    
    @SuppressWarnings("unchecked")
    @Override
	public List<T> getAllGeneric(){
    	String className = ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
    	Query query = em.createQuery("SELECT a FROM " + className + " a");
    	List<T> list = query.getResultList();
		return list;

    }

    
    
    
    
    
    
}
