package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.idao.ReunionInformationIDao;

@Remote (ReunionInformationIDao.class)
@Stateless
public class ReunionInformationDao extends GenericDao<ReunionInformation> implements ReunionInformationIDao {

	@PersistenceContext(unitName="daoProjet3PU")
	private EntityManager em;

	@Override
	@Transactional
	public void inscrireStagiaireRI(ReunionInformation ri, Stagiaire s) {
		Query query = em.createNativeQuery
		("INSERT INTO reunion_information_personne(ReunionInformation_id, stagiaires_id) VALUES (?,?)");
		query.setParameter(1, ri.getId());
		query.setParameter(2, s.getId());
		query.executeUpdate();
	}
	
	
	


}
