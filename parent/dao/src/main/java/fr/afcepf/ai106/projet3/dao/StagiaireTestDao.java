package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.idao.StagiaireTestIDao;

@Remote(StagiaireTestIDao.class)
@Stateless
public class StagiaireTestDao extends GenericDao<StagiaireTest> implements StagiaireTestIDao {

	
	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	@Override
	public StagiaireTest getStagiaireTestParStagiaire(Stagiaire s) {
		Query query = em.createQuery
		("SELECT st FROM StagiaireTest st WHERE st.stagiaire = :s");
		query.setParameter("s", s);
		StagiaireTest st = new StagiaireTest();
		List<StagiaireTest> listStagiaireTest = query.getResultList();
		if(listStagiaireTest.size()>0) {
			st = listStagiaireTest.get(listStagiaireTest.size()-1);
		}
		return st;
	}

	

}
