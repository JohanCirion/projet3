package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Entreprise;
import fr.afcepf.ai106.projet3.idao.EntrepriseIDao;

@Remote(EntrepriseIDao.class)
@Stateless
public class EntrepriseDao extends GenericDao<Entreprise> implements EntrepriseIDao {

	

}
