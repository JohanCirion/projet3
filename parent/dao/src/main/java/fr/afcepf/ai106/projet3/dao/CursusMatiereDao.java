package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.idao.CursusMatiereIDao;
import fr.afcepf.ai106.projet3.idao.MatiereIDao;

@Remote (CursusMatiereIDao.class)
@Stateless
public class CursusMatiereDao extends GenericDao<CursusMatiere> implements CursusMatiereIDao {

	
	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	
	@Override
	public CursusMatiere rechercherUnCursusMatiere(Cursus c, Matiere m) {
		Query query = em.createQuery("SELECT cm FROM CursusMatiere cm WHERE cm.cursus = :cursus and cm.matiere = :matiere");
		query.setParameter("cursus", c);
		query.setParameter("matiere", m);
		CursusMatiere cm = (CursusMatiere) query.getSingleResult();
		return cm;
	}


	@Override
	public List<CursusMatiere> rechercherParCursus(Cursus c) {
		Query query = em.createQuery("SELECT cm FROM CursusMatiere cm WHERE cm.cursus = :cursus ORDER BY ordre");
		query.setParameter("cursus", c);
		@SuppressWarnings("unchecked")
		List<CursusMatiere> listeCM = query.getResultList();
		return listeCM;
	}


	@Override
	public CursusMatiere rechercherParOrdre(Cursus c, Integer ordre) {
		Query query = em.createQuery("SELECT cm FROM CursusMatiere cm WHERE cm.cursus = :cursus and cm.ordre = :ordre");
		query.setParameter("cursus", c);
		query.setParameter("ordre", ordre);
		CursusMatiere cm = (CursusMatiere) query.getSingleResult();
		return cm;
	}


	@Override
	public void inverserOrdre(CursusMatiere cm1, CursusMatiere cm2) {
		Integer temp = cm2.getOrdre();
		cm2.setOrdre(cm1.getOrdre());
		cm1.setOrdre(temp);
		
		em.merge(cm1);
		em.merge(cm2);
		
	}







}
