package fr.afcepf.ai106.projet3.dao;



import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Intervenant;

import fr.afcepf.ai106.projet3.idao.IntervenantIDao;

@Remote (IntervenantIDao.class)
@Stateless
public class IntervenantDao extends GenericDao<Intervenant> implements IntervenantIDao {



	@PersistenceContext
	private EntityManager em; 
	
	@Override
	public Intervenant ajouter(Intervenant i) {
		em.persist(i);
		return i;
	}
	
	@Override
	public Intervenant modifier(Intervenant i) {
		em.merge(i);
		return i;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Intervenant> getAll() {

		Query query = em.createQuery("SELECT i FROM Intervenant i");
		List<Intervenant> intervenants = query.getResultList();
		
		return intervenants;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Intervenant> rechercherUnIntervenant(String name) {
		Query query = em.createQuery("SELECT i FROM Intervenant i WHERE i.nom LIKE CONCAT('%',:name,'%') OR i.prenom LIKE CONCAT('%',:name,'%')");
		query.setParameter("name", name);
		List<Intervenant> intervenants = query.getResultList();
		return intervenants;
	};

	
}
