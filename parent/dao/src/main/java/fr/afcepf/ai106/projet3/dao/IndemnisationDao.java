package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Indemnisation;
import fr.afcepf.ai106.projet3.idao.IndemnisationIDao;

@Remote(IndemnisationIDao.class)
@Stateless
public class IndemnisationDao extends GenericDao<Indemnisation> implements IndemnisationIDao {

	

}
