package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.StatutProfessionnel;
import fr.afcepf.ai106.projet3.idao.StatutProIDao;

@Remote(StatutProIDao.class)
@Stateless
public class StatutProDao extends GenericDao<StatutProfessionnel> implements StatutProIDao {


}
