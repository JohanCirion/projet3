package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.idao.DispositifIDao;

@Remote(DispositifIDao.class)
@Stateless
public class DispositifDao extends GenericDao<Dispositif> implements DispositifIDao {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	



}
