package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.idao.StagiairePromoIDao;

@Remote(StagiairePromoIDao.class)
@Stateless
public class StagiairePromoDao extends GenericDao<StagiairePromo> implements StagiairePromoIDao {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	@Override
	public Dispositif getDispositifByStagiaire(Stagiaire s) {
		Query query = em.createQuery		
		("SELECT sp.dispositif FROM StagiairePromo sp WHERE sp.stagiaire = :s");		
		query.setParameter("s", s);	
		Dispositif dispositif = new Dispositif();
		List<Dispositif> dispositifs = query.getResultList();
		if(dispositifs.size() > 0) {
			dispositif = dispositifs.get(0);
		}
		
		return dispositif;
	}

	@Override
	public Promotion getPromotionByStagiaire(Stagiaire s) {
		Query query = em.createQuery
		("SELECT sp.promotion FROM StagiairePromo sp WHERE sp.stagiaire = :s");
		query.setParameter("s", s);
		Promotion promo = new Promotion();
		List<Promotion> promos = query.getResultList();
		if(promos.size() > 0 ) {
			promo = promos.get(0);
		}
		
		return promo;
	}

	@Override
	public StagiairePromo enregisterUnStagiairePromo(Dispositif dispositif, Promotion promotion, Stagiaire stagiaire) {
		StagiairePromo sp = new StagiairePromo(dispositif, promotion, stagiaire);
		em.persist(sp);
		return sp;
	}

	

		
		
	

}
