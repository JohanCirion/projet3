package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.idao.ProvenanceIDao;

@Remote(ProvenanceIDao.class)
@Stateless
public class ProvenanceDao extends GenericDao<Provenance> implements ProvenanceIDao {



}
