package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai106.projet3.entities.Absence;
import fr.afcepf.ai106.projet3.idao.AbsenceIDao;

@Remote (AbsenceIDao.class)
@Stateless
public class AbsenceDao extends GenericDao<Absence> {

	@PersistenceContext(unitName="daoProjet3PU")
	private EntityManager em;
}
