package fr.afcepf.ai106.projet3.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Titre;
import fr.afcepf.ai106.projet3.idao.TitreIDao;

@Remote(TitreIDao.class)
@Stateless
public class TitreDao extends GenericDao<Titre> implements TitreIDao {



}
