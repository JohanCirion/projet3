package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.idao.MatiereIDao;

@Remote (MatiereIDao.class)
@Stateless
public class MatiereDao extends GenericDao<Matiere> implements MatiereIDao {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	@Override
	public List<Matiere> getAll() {
		Query query = em.createQuery("SELECT m FROM Matiere m" );
        
	       List<Matiere> matieres = query.getResultList();

		return matieres;
	}


}
