package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.idao.TestIDao;


@Remote (TestIDao.class)
@Stateless
public class TestDao extends GenericDao<Test> implements TestIDao {

	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	@Override
	public Test getTestByStagiaire(Stagiaire s) {
		Query query = em.createQuery
		("SELECT st.test FROM StagiaireTest st WHERE st.stagiaire = :s");
		query.setParameter("s", s);
		Test test = new Test();
		List<Test> listTest = query.getResultList();
		if(listTest.size()>0) {
			test = listTest.get(listTest.size()-1);
		}
		return test;
	}



}
