package fr.afcepf.ai106.projet3.dao;


import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Financement;
import fr.afcepf.ai106.projet3.idao.FinancementIDao;

@Remote(FinancementIDao.class)
@Stateless
public class FinancementDao extends GenericDao<Financement> implements FinancementIDao {

	
}
