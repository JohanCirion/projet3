package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai106.projet3.entities.Entretien;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;
import fr.afcepf.ai106.projet3.idao.EntretienIDao;

@Remote (EntretienIDao.class)
@Stateless
public class EntretienDao extends GenericDao<Entretien> implements EntretienIDao {

	
	@PersistenceContext (unitName = "daoProjet3PU")
    private EntityManager em;
	
	
	@Override
	public Entretien rechercherEntretienParStagiaire(Stagiaire s) {
		Entretien entretien = new Entretien();
		Query query = em.createQuery("SELECT e FROM Entretien e WHERE e.stagiaire = :stagiaire");
		query.setParameter("stagiaire", s);
		List<Entretien> entretiens = query.getResultList();
		if(entretiens.size() > 0) {
		entretien =  entretiens.get(entretiens.size()-1);
		}
		return entretien;
	}


}
