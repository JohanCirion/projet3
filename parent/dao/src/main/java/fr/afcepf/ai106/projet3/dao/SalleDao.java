package fr.afcepf.ai106.projet3.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Salle;
import fr.afcepf.ai106.projet3.idao.SalleIDao;


@Remote(SalleIDao.class)
@Stateless
public class SalleDao extends GenericDao<Salle> implements SalleIDao {

	

}
