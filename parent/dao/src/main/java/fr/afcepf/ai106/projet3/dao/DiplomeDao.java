package fr.afcepf.ai106.projet3.dao;


import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Diplome;
import fr.afcepf.ai106.projet3.idao.DiplomeIDao;


@Remote(DiplomeIDao.class)
@Stateless
public class DiplomeDao extends GenericDao<Diplome> implements DiplomeIDao {

	

}
