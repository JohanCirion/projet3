package fr.afcepf.ai106.projet3.business;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.ibusiness.AbsenceIBusiness;

@Remote(AbsenceIBusiness.class)
@Stateless
public class AbsenceBusiness implements AbsenceIBusiness {

}
