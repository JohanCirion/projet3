package fr.afcepf.ai106.projet3.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.ibusiness.MatiereIBusiness;
import fr.afcepf.ai106.projet3.idao.MatiereIDao;

@Remote(MatiereIBusiness.class)
@Stateless
public class MatiereBusiness implements MatiereIBusiness {

	@EJB
    private MatiereIDao proxyMatiereDao;
	
	@Override
	public Matiere ajouter(Matiere m) {
		m = proxyMatiereDao.ajouter(m);
		return m;
	}

	@Override
	public List<Matiere> getAll() {
		List<Matiere> matieres = proxyMatiereDao.getAll();
		return matieres;
	}

	@Override
	public Matiere rechercher(Integer i) {
		Matiere m = proxyMatiereDao.rechercher(i);
		return m;
	}

	@Override
	public Matiere modifier(Matiere m) {
		System.out.println("l'id de la matiere est " + m.getId());
		m = proxyMatiereDao.modifier(m);
		return m;
	}

}
