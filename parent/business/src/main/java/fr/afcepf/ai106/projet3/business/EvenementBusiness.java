package fr.afcepf.ai106.projet3.business;


import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.idao.ReunionInformationIDao;
import fr.afcepf.ai106.projet3.idao.StagiaireTestIDao;
import fr.afcepf.ai106.projet3.idao.TestIDao;


@Remote(EvenementIBusiness.class)
@Stateless
public class EvenementBusiness implements EvenementIBusiness {

	@EJB
	private ReunionInformationIDao proxyRIDao;
	
	@EJB
	private TestIDao proxyTestDao;
	
	@EJB
	private StagiaireTestIDao proxyStagiaireTestDao;
	
	@Override
	public ReunionInformation AjouterRI(ReunionInformation ri) {
		ri = proxyRIDao.ajouter(ri);
		return ri;
	}

	@Override
	public Test AjouterTest(Test t) {
		t = proxyTestDao.ajouter(t);
		return t;
	}

	@Override
	public List<Test> getAllTests() {
		return proxyTestDao.getAllGeneric();
	}

	@Override
	public Test getTestByStagiaire(Stagiaire s) {
		return proxyTestDao.getTestByStagiaire(s);
	}

	
	@Override
	public List<ReunionInformation> getAllRIFutur() {
		List<ReunionInformation> ListeNonTrie = proxyRIDao.getAllGeneric();
		List<ReunionInformation> ListeTrie = new ArrayList<>();
		for(ReunionInformation ri : ListeNonTrie) {
			if (ri.getDate().compareTo(GregorianCalendar.getInstance().getTime())>0) {
				ListeTrie.add(ri);
			}
		}
		return ListeTrie;
	}

	@Override
	public ReunionInformation rechercherRIParId(Integer id) {
		ReunionInformation ri = proxyRIDao.rechercher(id);
		return ri;
	}

	@Override
	public ReunionInformation modifierRI(ReunionInformation ri) {
		return proxyRIDao.modifier(ri);
	}

	@Override
	public List<ReunionInformation> getAllRI() {
		return proxyRIDao.getAllGeneric();
	}


	@Override
	public void inscrireStagiaireAUneRi(ReunionInformation ri, Stagiaire s) {
		proxyRIDao.inscrireStagiaireRI(ri, s);
			
	}
		//		Set<Stagiaire> list = new HashSet<Stagiaire>();
//		list = ri.getStagiaires();
//		list.add(s);
//		ri.setStagiaires(list);
//		ri = modifierRI(ri);
//		return ri;



	
	@Override
	public StagiaireTest getStagiaireTestParStagiaire(Stagiaire t) {
		return proxyStagiaireTestDao.getStagiaireTestParStagiaire(t);
	}

	
	
	
	
}
