package fr.afcepf.ai106.projet3.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.ibusiness.StagiairePromoIBusiness;
import fr.afcepf.ai106.projet3.idao.StagiairePromoIDao;

@Remote(StagiairePromoIBusiness.class)
@Stateless
public class StagiairePromoBusiness implements StagiairePromoIBusiness {

	@EJB
	private StagiairePromoIDao proxyStagiairePromoDao;
	
	
	public StagiairePromo ajouterStagPromo(StagiairePromo sp) {
		return proxyStagiairePromoDao.modifier(sp);
		
	}
}
