package fr.afcepf.ai106.projet3.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Diplome;
import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Entreprise;
import fr.afcepf.ai106.projet3.entities.Entretien;
import fr.afcepf.ai106.projet3.entities.Financement;
import fr.afcepf.ai106.projet3.entities.Indemnisation;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;
import fr.afcepf.ai106.projet3.entities.StatutProfessionnel;
import fr.afcepf.ai106.projet3.entities.Titre;
import fr.afcepf.ai106.projet3.ibusiness.StagiaireIBusiness;
import fr.afcepf.ai106.projet3.idao.CursusIDao;
import fr.afcepf.ai106.projet3.idao.DiplomeIDao;
import fr.afcepf.ai106.projet3.idao.DispositifIDao;
import fr.afcepf.ai106.projet3.idao.EntrepriseIDao;
import fr.afcepf.ai106.projet3.idao.EntretienIDao;
import fr.afcepf.ai106.projet3.idao.FinancementIDao;
import fr.afcepf.ai106.projet3.idao.IndemnisationIDao;
import fr.afcepf.ai106.projet3.idao.ProvenanceIDao;
import fr.afcepf.ai106.projet3.idao.StagiaireIDao;
import fr.afcepf.ai106.projet3.idao.StagiairePromoIDao;
import fr.afcepf.ai106.projet3.idao.StagiaireTestIDao;
import fr.afcepf.ai106.projet3.idao.StatutProIDao;
import fr.afcepf.ai106.projet3.idao.TitreIDao;

@Remote(StagiaireIBusiness.class)
@Stateless
public class StagiaireBusiness implements StagiaireIBusiness {

	@EJB
	private StagiaireIDao proxyStagiaireDao;
	
	@EJB
	private TitreIDao proxyTitreDao;
	
	@EJB
	private CursusIDao proxyCursusDao;
	
	@EJB
	private ProvenanceIDao proxyProvenanceDao;
	
	@EJB
	private DiplomeIDao proxyDiplomeDao;
	
	@EJB
	private DispositifIDao proxyDispositifDao;
	
	@EJB
	private FinancementIDao proxyFinancementDao;
	
	@EJB
	private EntrepriseIDao proxyEntrepriseDao;
	
	@EJB
	private StatutProIDao proxyStatutProDao;
	
	@EJB
	private IndemnisationIDao proxyIndemnisationDao;
	
	@EJB
	private StagiairePromoIDao proxyStagPromoDao;
	
	@EJB
	private EntretienIDao proxyEntretienDao;
	
	@EJB
	private StagiaireTestIDao proxyStagiaireTestDao;
	
	
	@Override
	public Stagiaire enregistrerUnStagiaire(Stagiaire s) {
		return proxyStagiaireDao.ajouter(s);
	}
	
	@Override
	public Stagiaire modifierUnStagiaire(Stagiaire s) {
		return proxyStagiaireDao.modifier(s);		
	}
	
	
	@Override
	public List<Stagiaire> getAllStagiaires() {		
		return proxyStagiaireDao.getAllStagiaires();
	}

	
	@Override
	public List<Stagiaire> rechercherUnStagiaire(String string) {
		return proxyStagiaireDao.rechercherUnStagiaire(string);
	}
	
	
	@Override
	public Stagiaire rechercher(int id) {
		return proxyStagiaireDao.rechercher(id);
	}


	@Override
	public List<Titre> getAllTitres() {
		return proxyTitreDao.getAllGeneric();
	}


	@Override
	public List<Cursus> getAllCursus() {
		return proxyCursusDao.getAllGeneric();
	}


	@Override
	public List<Provenance> getAllProvenances() {
		return proxyProvenanceDao.getAllGeneric();
	}


	@Override
	public List<Diplome> getAllDiplomes() {
		return proxyDiplomeDao.getAllGeneric();
	}


	@Override
	public List<Dispositif> getAllDispositifs() {
		return proxyDispositifDao.getAllGeneric();
	}


	@Override
	public List<Financement> getAllFinancements() {
		return proxyFinancementDao.getAllGeneric();
	}


	@Override
	public List<Entreprise> getAllEntreprises() {
		return proxyEntrepriseDao.getAllGeneric();
	}


	@Override
	public List<StatutProfessionnel> getAllStatutPro() {
		return proxyStatutProDao.getAllGeneric();
	}


	@Override
	public List<Indemnisation> getAllIndemnisations() {
		return proxyIndemnisationDao.getAllGeneric();
	}


	@Override
	public List<StagiairePromo> getAllStagPromos() {
		return proxyStagPromoDao.getAllGeneric();	
	}


	@Override
	public Dispositif getDispositifByStagiaire(Stagiaire s) {
		return proxyStagPromoDao.getDispositifByStagiaire(s);
	}




	
	@Override
	public StagiairePromo enregisterUnDispositif(StagiairePromo sp) {
		return proxyStagPromoDao.ajouter(sp);		
	}


	@Override
	public StagiairePromo enregisterUnStagiairePromo(Dispositif dispositif, Promotion promotion, Stagiaire stagiaire) {
		return proxyStagPromoDao.enregisterUnStagiairePromo(dispositif, promotion, stagiaire);
		
	}

	@Override
	public List<Entretien> getAllEntretien() {
		
		return proxyEntretienDao.getAllGeneric();
	}

	@Override
	public Entretien getEntretienParStagiaire(Stagiaire s) {
		
		return proxyEntretienDao.rechercherEntretienParStagiaire(s);
	}

	@Override
	public StagiaireTest EnregistrerUnStagiaireTest(StagiaireTest st) {
		return proxyStagiaireTestDao.modifier(st);
		
	}

	@Override
	public void enregistrerUnStagiaireAUnEntretien(Stagiaire s, Entretien e) {
		e.setStagiaire(s);
		proxyEntretienDao.modifier(e);
		
	}
	
	

	@Override
	public List<Stagiaire> getAllCandidats() {
		return proxyStagiaireDao.getAllCandidats();
	}

	@Override
	public Provenance demoProvenance() {
		return proxyStagiaireDao.demoProvenance();
	}

	

}
