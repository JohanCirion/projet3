package fr.afcepf.ai106.projet3.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Salle;
import fr.afcepf.ai106.projet3.ibusiness.LogistiqueIBusiness;
import fr.afcepf.ai106.projet3.idao.SalleIDao;


@Remote (LogistiqueIBusiness.class)
@Stateless
public class LogistiqueBusiness implements LogistiqueIBusiness {
	
	@EJB
	private SalleIDao proxySalleDao;

	@Override
	public List<Salle> getAllSalle() {
		List<Salle> salles = proxySalleDao.getAllGeneric();
		return salles;
	}

}
