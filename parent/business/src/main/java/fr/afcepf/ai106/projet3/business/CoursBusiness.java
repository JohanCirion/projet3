package fr.afcepf.ai106.projet3.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.CoursIBusiness;
import fr.afcepf.ai106.projet3.idao.CoursIDao;

@Remote(CoursIBusiness.class)
@Stateless
public class CoursBusiness {

	@EJB
	private CoursIDao proxyCoursDao;
	
	public List<Cours> getCoursByPromoMatiere(Promotion p, Matiere m){		
		List<Cours> courses = proxyCoursDao.getCoursByPromoMatiere(p, m);				
		return courses;
		
	}
	
}
