package fr.afcepf.ai106.projet3.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.idao.CursusIDao;
import fr.afcepf.ai106.projet3.idao.CursusMatiereIDao;


@Remote(CursusIBusiness.class)
@Stateless
public class CursusBusiness implements CursusIBusiness {

	
	@EJB
	private CursusIDao proxyCursusDao;
	
	@EJB
	private CursusMatiereIDao proxyCursusMatiereDao;
	
	@Override
	public List<Cursus> getAll() {
		List<Cursus> cursus = proxyCursusDao.getAll();
		return cursus;
	}

	@Override
	public void supprimerMatiereDUnCursus(Cursus c, Matiere m) {
		CursusMatiere cm = proxyCursusMatiereDao.rechercherUnCursusMatiere(c, m);
		proxyCursusMatiereDao.supprimer(cm);
		List<CursusMatiere> ListeCM = proxyCursusMatiereDao.rechercherParCursus(c);
		int i = 1;
		for (CursusMatiere cursusMatiere : ListeCM) {
			cursusMatiere.setOrdre(i);
			proxyCursusMatiereDao.modifier(cursusMatiere);
			i++;
			
		}
	}

	@Override
	public void ajouterMatiereAUnCursus(Cursus c, Matiere m, Integer duree, Integer ordre) {
		CursusMatiere cm = new CursusMatiere();
		cm.setCursus(c);
		cm.setMatiere(m);
		cm.setDuree(duree);
		cm.setOrdre(ordre);
		proxyCursusMatiereDao.ajouter(cm);
		
	}

	@Override
	public Cursus AjouterCursus(Cursus c) {
		return proxyCursusDao.ajouter(c);
		
	}

	@Override
	public Cursus rechercher(Integer id) {
		Cursus c = proxyCursusDao.rechercher(id);
		return c;
	}

	@Override
	public List<CursusMatiere> CursusMatiereDUnCursus(Cursus c) {
		List<CursusMatiere> ListeCM = proxyCursusMatiereDao.rechercherParCursus(c);
		return ListeCM;
	}

	@Override
	public void monterDansUnCursus(CursusMatiere cm1) {
		if(cm1.getOrdre() != 1) {
		CursusMatiere cm2 = proxyCursusMatiereDao.rechercherParOrdre(cm1.getCursus(), cm1.getOrdre() - 1);
		proxyCursusMatiereDao.inverserOrdre(cm1, cm2);
		}
		
	}

	@Override
	public void descendreDansUnCursus(CursusMatiere cm1) {
		List<CursusMatiere> ListeCM = proxyCursusMatiereDao.rechercherParCursus(cm1.getCursus());
		if(cm1.getOrdre() != ListeCM.size()) {
		CursusMatiere cm2 = proxyCursusMatiereDao.rechercherParOrdre(cm1.getCursus(), cm1.getOrdre() + 1);
		proxyCursusMatiereDao.inverserOrdre(cm1, cm2);
		}
	}

	@Override
	public void validerUnCursus(Cursus c) {
		c.setDateValidation(new Date());
		c.setDateInvalid(null);
		proxyCursusDao.modifier(c);
		
	}

	@Override
	public void invaliderUnCursus(Cursus c) {
		c.setDateValidation(null);
		c.setDateInvalid(new Date());
		proxyCursusDao.modifier(c);
		
	}

	@Override
	public List<Cursus> tousLesCursusValides() {
		return proxyCursusDao.getCursusValides();
	}

	@Override
	public List<Cursus> getInvalidCursi() {
		return proxyCursusDao.getInvalidCursi();
	}
}
