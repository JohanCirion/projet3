package fr.afcepf.ai106.projet3.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.GregorianCalendar;

import java.util.Calendar;
import java.util.Date;

import java.util.List;
import java.util.Set;


import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;
import fr.afcepf.ai106.projet3.idao.CoursIDao;
import fr.afcepf.ai106.projet3.idao.CursusIDao;
import fr.afcepf.ai106.projet3.idao.CursusMatiereIDao;
import fr.afcepf.ai106.projet3.idao.PromotionIDao;
import fr.afcepf.ai106.projet3.idao.StagiairePromoIDao;

@Remote(PromotionIBusiness.class)
@Stateless
public class PromotionBusiness implements PromotionIBusiness {

	@EJB
	private PromotionIDao proxyPromotionDao;
	


	@EJB
	private StagiairePromoIDao proxyStagiairePromoDao;

	@EJB
	private CoursIDao proxyCoursDao;
	
	@EJB
	private CursusIDao proxyCursusDao;

	
	@EJB
	private CursusMatiereIDao proxyCursusMatiereDao;
	
	
	@Override
	public List<Promotion> getAllPromotions() {
		return proxyPromotionDao.getAllPromotions();
	}

	@Override
	public List<Promotion> getAllFuturesPromotions() {
		List<Promotion> allPromos = proxyPromotionDao.getAllPromotions();
		List<Promotion> enCours = new ArrayList<Promotion>();
		for (Promotion promotion : allPromos) {
			if (promotion.getDateDebut().compareTo(GregorianCalendar.getInstance().getTime())>0) {
				enCours.add(promotion);
			}
		}
		return enCours;	
	}
	
	@Override
	public List<Stagiaire> getStagiairesByPromo(Promotion p) {
		return proxyPromotionDao.getStagiairesbyPromo(p);
	}

	@Override
	public Promotion getPromotionByStagiaire(Stagiaire s) {
		return proxyStagiairePromoDao.getPromotionByStagiaire(s);
	}

	public int getDureeCursus(Cursus c) {		
		return proxyCursusDao.getDureeCursus(c);
	}
	
	@Override
	public List<Cours> getCoursMatiere(CursusMatiere cm) {
		List<Cours> result = new ArrayList<>();
		for (int i=0; i < cm.getDuree(); i++) {
			Cours cours = new Cours();
			cours.setMatiere(cm.getMatiere());
			result.add(cours);
		}
		return result;
	}
	
	@SuppressWarnings("unused")
	public List<CursusMatiere> getMatieres(Promotion p){
		List<CursusMatiere> result = new ArrayList<>();
		result = proxyCursusDao.getMatieres(p.getCursus());
		return result;
	}
	
	@Override
	public Promotion ajouter(Promotion p) {
		p = proxyPromotionDao.ajouter(p);
		List<CursusMatiere> matieresCursus = proxyCursusDao.getMatieres(p.getCursus());
		Date date = p.getDateDebut();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		date = c.getTime();
		boolean matin = true;
		for (CursusMatiere cm: matieresCursus) {
			List<Cours> temp = getCoursMatiere(cm);
			for (Cours cours : temp) {
				if(date.getDay() == 6) {
					c.add(Calendar.DATE, 2);
					date = c.getTime();
					cours.setDateDebut(date);
				} else {
					cours.setPromotion(p);
					cours.setDateDebut(date);
					if (matin) {
						cours.setMatin(matin);					
						matin = false;
					} else {
						cours.setMatin(matin);
						c.add(Calendar.DATE, 1);
						date = c.getTime();
						matin = true;
					}
				}
				
				cours = proxyCoursDao.ajouter(cours);
			}
		}
		return p;
	}
	

	@Override
	public Date calculDateFinCours(Cours c) {
		Date result = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh/mm");
		Calendar calendar = Calendar.getInstance();
		Date debut = c.getDateDebut();
		calendar.setTime(debut);
		calendar.add(Calendar.HOUR_OF_DAY, 4);
		result = calendar.getTime();
		return result;
	}
	
	@Override
	public Promotion getPromoById(Integer id) {
		return proxyPromotionDao.rechercher(id);
	}

	@Override
	public List<Cours> getAllCours() {
		return proxyCoursDao.getAllGeneric();
	}

	@Override
	public List<Cours> tousLesCours(Promotion p) {
		return proxyPromotionDao.getAllCours(p);
	}

	@Override
	public List<CursusMatiere> getAllCursusMatiere() {
		List<CursusMatiere> listeCM = proxyCursusMatiereDao.getAllGeneric();
		return listeCM;
	}

	@Override
	public Cours updateCours(Cours c) {
		return proxyCoursDao.modifier(c);
	}

	@Override
	public Cours getById(int id) {
		return proxyCoursDao.rechercher(id);
	}

	@Override
	public Integer calculNumPromo(Cursus c) {
		return proxyPromotionDao.getNumPromo(c);
	}

	@Override
	public Date endDate(Promotion p) {
		return proxyPromotionDao.endDate(p);
	}




}
