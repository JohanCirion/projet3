package fr.afcepf.ai106.projet3.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.IntervenantIBusiness;
import fr.afcepf.ai106.projet3.idao.CoursIDao;
import fr.afcepf.ai106.projet3.idao.IntervenantIDao;
import fr.afcepf.ai106.projet3.idao.PromotionIDao;

@Remote (IntervenantIBusiness.class)
@Stateless
public class IntervenantBusiness implements IntervenantIBusiness {

	@EJB
	private IntervenantIDao proxyIntervenantDao;

	@EJB
	private CoursIDao proxyCoursDao;
	
	@EJB
	private PromotionIDao proxyPromoDao;
	
	
	@Override
	public Intervenant ajouter(Intervenant i) {
		Intervenant result = null;
		result = proxyIntervenantDao.ajouter(i);		
		return result;
	}

	@Override
	public Intervenant modifier(Intervenant i) {	 
		return proxyIntervenantDao.modifier(i);
	}
	
	
	@Override
	public List<Intervenant> getAll() {
		List<Intervenant> intervenants = proxyIntervenantDao.getAll();		
		return intervenants;
	}

	@Override
	public Intervenant rechercher(Integer i) {
		Intervenant interv = proxyIntervenantDao.rechercher(i);
		return interv;
	}

	@Override 
	public Cours affecterIntervenantCours(Cours c, Intervenant i) {
			c.setIntervenant(i);	
	
			return proxyCoursDao.modifier(c);	
		}
				
	

	@Override
	public List<Cours> getAllCours() {
		List<Cours> courses = proxyCoursDao.getAllGeneric();		
		return courses;
	}


	@Override
	public List<Intervenant> rechercherUnIntervenant(String string) {	
		return proxyIntervenantDao.rechercherUnIntervenant(string);
	}

	@Override
	public Intervenant getIntervenantById(Integer id) {	
		return proxyIntervenantDao.rechercher(id);
	}

	
}
