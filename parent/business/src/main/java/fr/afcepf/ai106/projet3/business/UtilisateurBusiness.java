package fr.afcepf.ai106.projet3.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.management.relation.Role;

import fr.afcepf.ai106.projet3.entities.Administratif;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Profil;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;
import fr.afcepf.ai106.projet3.ibusiness.UtilisateurIBusiness;
import fr.afcepf.ai106.projet3.idao.ProfilIDao;
import fr.afcepf.ai106.projet3.idao.StagiaireIDao;
import fr.afcepf.ai106.projet3.idao.UtilisateurIDao;

@Remote(UtilisateurIBusiness.class)
@Stateless
public class UtilisateurBusiness implements UtilisateurIBusiness {

	@EJB
	private UtilisateurIDao proxyUtilisateurDao;
	
	@EJB
	private StagiaireIDao proxyStagiaireDao;
	
	@EJB
	private ProfilIDao proxyProfilDao;
	
	@Override
	public Utilisateur Authenticate(String login, String password) {
		Utilisateur u = proxyUtilisateurDao.Authenticate(login, password);
		return u;
	}


	@Override
	public Stagiaire getStagiaireByUtilisateur(Utilisateur u) {
		return proxyUtilisateurDao.getStagiaireByUtilisateur(u);
	}


	@Override
	public void createLoginAndPasswordforStagiaire(Utilisateur u, Stagiaire s) {
		if(u.equals(null)) {
		u = proxyUtilisateurDao.modifier(u);
		} else {
			u = proxyUtilisateurDao.ajouter(u);
		}
		s.setUtilisateur(u);
		proxyStagiaireDao.modifier(s);		
	}


	@Override
	public List<Stagiaire> getStagiaireByName(String name) {
		return proxyUtilisateurDao.getStagiairebyName(name);
	}


	@Override
	public List<Intervenant> getIntervenantByName(String name) {
		return proxyUtilisateurDao.getIntervenantbyName(name);
	}


	@Override
	public List<Administratif> getAdministratifByName(String name) {
		return proxyUtilisateurDao.getAdministratifbyName(name);
	}


	@Override
	public List<Profil> getAllProfil() {
		return proxyProfilDao.getAllGeneric();
	}


	@Override
	public Intervenant getIntervenantByUtilisateur(Utilisateur u) {
		return proxyUtilisateurDao.getIntervByUtilisateur(u);
	}


	@Override
	public Administratif getAdministratifByUtilisateur(Utilisateur u) {
		return proxyUtilisateurDao.getAdminByUtilisateur(u);
	}









}
