package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
public class Absence implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L; 

	/**
     * Default constructor
     */
    public Absence() {
    }

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @Column
    private String motif;

    /**
     * 
     */
    @Column
    private Boolean justifiee;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Stagiaire stagiaire;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Cours cours;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMotif() {
		return motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	public Boolean getJustifiee() {
		return justifiee;
	}

	public void setJustifiee(Boolean justifiee) {
		this.justifiee = justifiee;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}

	public Cours getCours() {
		return cours;
	}

	public void setCours(Cours cours) {
		this.cours = cours;
	}

}