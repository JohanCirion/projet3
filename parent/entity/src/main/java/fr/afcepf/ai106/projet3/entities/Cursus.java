package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
public class Cursus implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L; 

	/**
     * Default constructor
     */
    public Cursus() {
    }

    public Cursus(String nom) {
    	this.nom = nom;
    }
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @Column
    private String nom;

    /**
     * 
     */
    @Column
    private String code;

    /**
     * 
     */
    @Column
    private Date dateValidation;

    /**
     * 
     */
    @Column
    private Date dateInvalid;

    /**
     * 
     */
    @Column
    private Integer dureeTheoriq;
    
    
    

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(Date dateValidation) {
		this.dateValidation = dateValidation;
	}

	public Date getDateInvalid() {
		return dateInvalid;
	}

	public void setDateInvalid(Date dateInvalid) {
		this.dateInvalid = dateInvalid;
	}

	public Integer getDureeTheoriq() {
		return dureeTheoriq;
	}

	public void setDureeTheoriq(Integer dureeTheoriq) {
		this.dureeTheoriq = dureeTheoriq;
	}

	@Override
	public String toString() {
		return "Cursus [id=" + id + ", nom=" + nom + ", code=" + code + ", dateValidation=" + dateValidation
				+ ", dateInvalid=" + dateInvalid + ", dureeTheoriq=" + dureeTheoriq + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((dateInvalid == null) ? 0 : dateInvalid.hashCode());
		result = prime * result + ((dateValidation == null) ? 0 : dateValidation.hashCode());
		result = prime * result + ((dureeTheoriq == null) ? 0 : dureeTheoriq.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cursus other = (Cursus) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (dateInvalid == null) {
			if (other.dateInvalid != null)
				return false;
		} else if (!dateInvalid.equals(other.dateInvalid))
			return false;
		if (dateValidation == null) {
			if (other.dateValidation != null)
				return false;
		} else if (!dateValidation.equals(other.dateValidation))
			return false;
		if (dureeTheoriq == null) {
			if (other.dureeTheoriq != null)
				return false;
		} else if (!dureeTheoriq.equals(other.dureeTheoriq))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}



	




}