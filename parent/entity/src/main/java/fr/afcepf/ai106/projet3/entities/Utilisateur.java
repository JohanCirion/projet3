package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 */
@Entity
public class Utilisateur implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L; 





	/**
     * Default constructor
     */
    public Utilisateur() {
    }

    /**
     * 
     */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @Column
    private String login;

    /**
     * 
     */
    @Column
    private String password;





    /**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Profil profil;





	public Integer getId() {
		return id;
	}





	public void setId(Integer id) {
		this.id = id;
	}





	public String getLogin() {
		return login;
	}





	public void setLogin(String login) {
		this.login = login;
	}





	public String getPassword() {
		return password;
	}





	public void setPassword(String password) {
		this.password = password;
	}





	public Profil getProfil() {
		return profil;
	}





	public void setProfil(Profil profil) {
		this.profil = profil;
	}
    
    

}