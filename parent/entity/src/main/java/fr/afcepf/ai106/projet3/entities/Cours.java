package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
public class Cours implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;   

	/**
     * Default constructor
     */
	   public Cours() {
			super();
			// TODO Auto-generated constructor stub
		}

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

 

	/**
     * 
     */
    @Column
    private Date dateDebut;

    /**
     * 
     */
    @Column
    private Boolean matin;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Intervenant intervenant;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Matiere matiere;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Salle salle;
    
    
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Promotion promotion;
    
    

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getMatin() {
		return matin;
	}

	public void setMatin(Boolean matin) {
		this.matin = matin;
	}

	public Intervenant getIntervenant() {
		return intervenant;
	}

	public void setIntervenant(Intervenant intervenant) {
		this.intervenant = intervenant;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}


}