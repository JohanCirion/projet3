package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * 
 */
@Entity
public class Promotion  implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L; 

	/**
     * Default constructor
     */
    public Promotion() {
    }

    public Promotion(String nom) {
    	this.nom = nom;
    }

    
    
    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @Column
    private Date dateDebut;

    /**
     * 
     */
    @Column
    private Date dateAnnul;
    
    
    /**
     * Ajout manuel d'une liste de cours
     */
    @OneToMany
    @JoinColumn (referencedColumnName = "id")
    private List<Cours> cours;
    
    /**
     *  Ajout du nom de la promotion
     */
    @Column
    private String nom;
  
	@Column
    private Integer numero;

    public List<Cours> getCours() {
		return cours;
	}

	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}

	/**
     * 
     */
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Cursus cursus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateAnnul() {
		return dateAnnul;
	}

	public void setDateAnnul(Date dateAnnul) {
		this.dateAnnul = dateAnnul;
	}

	public Cursus getCursus() {
		return cursus;
	}

	public void setCursus(Cursus cursus) {
		this.cursus = cursus;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
    
	
	
   
	
    

}