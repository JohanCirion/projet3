package fr.afcepf.ai106.projet3.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class StagiaireTest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Stagiaire stagiaire;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Test test;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}
    
    
}
