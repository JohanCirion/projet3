package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 */
@Entity
public class Test implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L; 

	/**
     * Default constructor
     */
    public Test() {
    }

    
    public Test(String nom) {
    	this.nom = nom;
    }
    
    /**
     * 
     */
    @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @Column
    private String nom;

    /**
     * 
     */
    @Column
    private Date dateTest;

    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Salle salle;
   

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public Date getDateTest() {
		return dateTest;
		
	}

	public void setDateTest(Date dateTest) {
		this.dateTest = dateTest;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}


    

}