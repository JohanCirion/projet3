package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 */
@Entity
public class Matiere  implements Serializable  {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
  
	/**
     * Default constructor
     */
    public Matiere() {
    }

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @Column
    private String nom;

    /**
     * 
     */
    @Column
    private Date dateInvalid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateInvalid() {
		return dateInvalid;
	}

	public void setDateInvalid(Date dateInvalid) {
		this.dateInvalid = dateInvalid;
	}

    





}