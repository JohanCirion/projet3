package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class StagiairePromo implements Serializable {


	private static final long serialVersionUID = 1L; 


    @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Stagiaire stagiaire;


    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Promotion promotion;

 
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Dispositif dispositif;

    

    public StagiairePromo() {
    }
    
    public StagiairePromo(Dispositif d) {
    	this.dispositif = d;
    }
    
    public StagiairePromo(Dispositif dispositif, Promotion promotion, Stagiaire stagiaire) {
    	this.dispositif = dispositif;
    	this.promotion = promotion;
    	this.stagiaire = stagiaire;
    }
    
    public StagiairePromo(int idDispo, int idPromo, int idStag) {
    	this.dispositif.setId(idDispo);
    	this.promotion.setId(idPromo);
    	this.stagiaire.setId(idStag);
    }
    
    public StagiairePromo(Dispositif d, Stagiaire s) {
    	this.dispositif = d;
    	this.stagiaire = s;
    }
    
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public Dispositif getDispositif() {
		return dispositif;
	}

	public void setDispositif(Dispositif dispositif) {
		this.dispositif = dispositif;
	}

    
}