package fr.afcepf.ai106.projet3.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;



/**
 * 
 */
@Entity
@DiscriminatorValue("Stagiaire")
public class Stagiaire extends Personne implements Serializable   {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; 

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String photo;
	
	@Column
	private String cv;
	
	@Column
	private String lieuNaissance;
	
	@Column
	private String paysNaissance;
	
	@Column
	private String expPro;
	
	@Column
	private String dureeExpPro;
	
	@Column
	private String idPoleEmp;
	
	@Column
	private Date datePoleEmp;
	
    @Column
    private String noteLogique;

    @Column
    private String noteRaisonnement;
	
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private Cursus cursusVise;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private Diplome diplome;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private Financement financement;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private Entreprise entreprise;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private Provenance provenance;
    
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private StatutProfessionnel statut;
	
    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
	private Indemnisation indemnisation;


	public Stagiaire() {
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getCv() {
		return cv;
	}


	public void setCv(String cv) {
		this.cv = cv;
	}


	public String getLieuNaissance() {
		return lieuNaissance;
	}


	public void setLieuNaissance(String lieuNaissance) {
		this.lieuNaissance = lieuNaissance;
	}


	public String getPaysNaissance() {
		return paysNaissance;
	}


	public void setPaysNaissance(String paysNaissance) {
		this.paysNaissance = paysNaissance;
	}


	public String getExpPro() {
		return expPro;
	}


	public void setExpPro(String expPro) {
		this.expPro = expPro;
	}


	public String getDureeExpPro() {
		return dureeExpPro;
	}


	public void setDureeExpPro(String dureeExpPro) {
		this.dureeExpPro = dureeExpPro;
	}


	public String getIdPoleEmp() {
		return idPoleEmp;
	}


	public void setIdPoleEmp(String idPoleEmp) {
		this.idPoleEmp = idPoleEmp;
	}


	public Date getDatePoleEmp() {
		return datePoleEmp;
	}


	public void setDatePoleEmp(Date datePoleEmp) {
		this.datePoleEmp = datePoleEmp;
	}


	public Cursus getCursusVise() {
		return cursusVise;
	}


	public void setCursusVise(Cursus cursusVise) {
		this.cursusVise = cursusVise;
	}


	public Diplome getDiplome() {
		return diplome;
	}


	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}


	public Financement getFinancement() {
		return financement;
	}


	public void setFinancement(Financement financement) {
		this.financement = financement;
	}


	public Entreprise getEntreprise() {
		return entreprise;
	}


	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}


	public Provenance getProvenance() {
		return provenance;
	}


	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}


	public StatutProfessionnel getStatut() {
		return statut;
	}


	public void setStatut(StatutProfessionnel statut) {
		this.statut = statut;
	}


	public Indemnisation getIndemnisation() {
		return indemnisation;
	}


	public void setIndemnisation(Indemnisation indemnisation) {
		this.indemnisation = indemnisation;
	}


	@Override
	public String toString() {
		return "" + id + "";
	}


	public String getNoteLogique() {
		return noteLogique;
	}


	public void setNoteLogique(String noteLogique) {
		this.noteLogique = noteLogique;
	}


	public String getNoteRaisonnement() {
		return noteRaisonnement;
	}


	public void setNoteRaisonnement(String noteRaisonnement) {
		this.noteRaisonnement = noteRaisonnement;
	}

	
	
	

}