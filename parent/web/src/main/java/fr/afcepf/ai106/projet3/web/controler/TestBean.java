package fr.afcepf.ai106.projet3.web.controler;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai106.projet3.entities.Salle;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.LogistiqueIBusiness;

@ManagedBean (name= "mbTest")
@ViewScoped
public class TestBean {

	private Date dateDuTest;
	private Salle salleDuTest = new Salle();
	private List<Salle> salles;
	private String message;
	
	
	@EJB
    private EvenementIBusiness proxyEvenenementBusiness;
	
	@EJB
    private LogistiqueIBusiness proxyLogistiqueBusiness;
	
	@PostConstruct
	public void Init() {
		salles= proxyLogistiqueBusiness.getAllSalle();
		
	}
	
	public void AjouterTest() {
		Test t = new Test();
		t.setDateTest(dateDuTest);
		System.out.println(dateDuTest);
		t.setSalle(salleDuTest);
		t = proxyEvenenementBusiness.AjouterTest(t);
		message = "Session de test programmée";
//		return  "/TestAjoute.xhtml?faces-redirect=true";
	}

	public Date getDateDuTest() {
		return dateDuTest;
	}

	public void setDateDuTest(Date dateDuTest) {
		this.dateDuTest = dateDuTest;
	}

	public Salle getSalleDuTest() {
		return salleDuTest;
	}

	public void setSalleDuTest(Salle salleDuTest) {
		this.salleDuTest = salleDuTest;
	}

	public List<Salle> getSalles() {
		return salles;
	}

	public void setSalles(List<Salle> salles) {
		this.salles = salles;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
}
