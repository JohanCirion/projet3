package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.ibusiness.IntervenantIBusiness;

@ManagedBean(name="mbRechercheIntervenant")
@ViewScoped
public class RechercheIntervenantBean implements Serializable {


	private static final long serialVersionUID = 1L;
	
	@EJB
	private IntervenantIBusiness proxyIntervenantBusiness;

	private String nomRecherche;
	private String message = "";
	private Intervenant interv = new Intervenant();
	private List<Intervenant> intervenants; 
	
	
	@PostConstruct
	public void init() {
		intervenants = proxyIntervenantBusiness.getAll();	
		
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer id=0;
		if(params.get("id") != null) {
		id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
		interv = proxyIntervenantBusiness.getIntervenantById(id);
		}
	}
	
	public String afficherIntervenant(Integer id) {
		
		return "/ModifierIntervenant.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public void rechercherUnIntervenant() {
		intervenants = new ArrayList<>();
		intervenants = proxyIntervenantBusiness.rechercherUnIntervenant(nomRecherche);
		if (intervenants.size() == 0) {
			message = "Aucune correspondance";
			intervenants = new ArrayList<Intervenant>();
		}	
		for (Intervenant interv : intervenants) {
			message = "Résultat de la recherche : " + intervenants.size() + " intervenant(s) trouvé(s)";
		}
		System.out.println("recherche intervenant " + intervenants.size());
	}
	
	public void preRender() {
		intervenants = proxyIntervenantBusiness.getAll();	
		message = "";
		nomRecherche="";
	}

	public String getNomRecherche() {
		return nomRecherche;
	}

	public void setNomRecherche(String nomRecherche) {
		this.nomRecherche = nomRecherche;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Intervenant getInterv() {
		return interv;
	}

	public void setInterv(Intervenant interv) {
		this.interv = interv;
	}

	public List<Intervenant> getIntervenants() {
		return intervenants;
	}

	public void setIntervenants(List<Intervenant> intervenants) {
		this.intervenants = intervenants;
	}
	
	
}
