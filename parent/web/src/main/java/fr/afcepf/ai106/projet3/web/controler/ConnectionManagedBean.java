package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai106.projet3.entities.Administratif;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.UtilisateurIBusiness;

@ManagedBean (name= "mbConnection")
@SessionScoped
public class ConnectionManagedBean  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private Utilisateur utilisateur;
	private String login;
	private String password;
	private List<String> roles;
	private String role;
	private String message;
	private Stagiaire stagiaire;
	private Intervenant intervenant;
	private Administratif administratif;
	
	
	
	@EJB
    private UtilisateurIBusiness proxyUtilisateurBusiness;
	
	
	  @PostConstruct
	    public void init() {
		  roles = new ArrayList<String>();
		  roles.add("Stagiaire");
		  roles.add("Intervenant");
		  roles.add("Administrateur");
	  }
	
	
	  public String seConnecter() {
		  String retour = "";
		  switch(role) {
		  case "Stagiaire":
			  if(proxyUtilisateurBusiness.Authenticate(login, password) != null) {
				  utilisateur = proxyUtilisateurBusiness.Authenticate(login, password);
				  stagiaire = proxyUtilisateurBusiness.getStagiaireByUtilisateur(utilisateur);  
				  if(utilisateur.getProfil().getNom() != "Stagiaire") {
					  retour = "/AccueilStagiaire.xhtml?faces-redirect=true";
				  }

			  }
			 
		    break;
		  case "Intervenant":
			  if(proxyUtilisateurBusiness.Authenticate(login, password) != null) {
				  utilisateur = proxyUtilisateurBusiness.Authenticate(login, password);
				  intervenant = proxyUtilisateurBusiness.getIntervenantByUtilisateur(utilisateur);
				  if(utilisateur.getProfil().getNom() != "Intervenant") {
				  retour = "/AccueilIntervenant.xhtml?faces-redirect=true";
				  }
			  }
		    break;
		  case "Administrateur":
			  if(proxyUtilisateurBusiness.Authenticate(login, password) != null) {
				  utilisateur = proxyUtilisateurBusiness.Authenticate(login, password);
				  administratif = proxyUtilisateurBusiness.getAdministratifByUtilisateur(utilisateur);
				  if(utilisateur.getProfil().getNom() != "Administratif") {
				  retour = "/Accueil.xhtml?faces-redirect=true";
				  }
			  }
		    break;
		  }
		  message="Veuillez rentrer un rôle, un login et un mot de passe correct.";
		  return retour;

	  }
	
	
	  public String seDeconnecter() {
		  return "Connection.xhtml?faces-redirect=true";
	  }
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}


	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public Intervenant getIntervenant() {
		return intervenant;
	}


	public void setIntervenant(Intervenant intervenant) {
		this.intervenant = intervenant;
	}


	public Administratif getAdministratif() {
		return administratif;
	}


	public void setAdministratif(Administratif administratif) {
		this.administratif = administratif;
	}
	
	
	
	
}
