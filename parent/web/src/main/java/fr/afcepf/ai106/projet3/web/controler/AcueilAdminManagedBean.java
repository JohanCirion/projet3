package fr.afcepf.ai106.projet3.web.controler;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiaireIBusiness;

@ManagedBean (name = "mbAdminHome")
@ViewScoped
public class AcueilAdminManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Promotion promo = new Promotion();
	private List<Promotion> promos;
	private String msg = "";
	
	private Cursus cursus = new Cursus();
	private List<Cursus> validCursi;
	private List<Cursus> unValidCursi;
	
	private List<Stagiaire> allCandidates;
	
	@EJB
	private PromotionIBusiness proxyPromoBusiness;
	
	@EJB
	private CursusIBusiness proxyCursusBusiness;
	
	@EJB
	private StagiaireIBusiness proxyStagiaireBusiness;
	
	
	public Date promoEndDate(Promotion p) {		
		return proxyPromoBusiness.endDate(p);
	}
	
	@PostConstruct
	public void onInit() {
		validCursi = proxyCursusBusiness.tousLesCursusValides();
		unValidCursi = proxyCursusBusiness.getInvalidCursi();
		promos = proxyPromoBusiness.getAllPromotions();
		allCandidates = proxyStagiaireBusiness.getAllCandidats();
		msg = "";
	}
	
	public Promotion getPromo() {
		return promo;
	}

	public List<Promotion> getPromos() {
		return promos;
	}

	public void setPromos(List<Promotion> promos) {
		this.promos = promos;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Cursus getCursus() {
		return cursus;
	}

	public void setCursus(Cursus cursus) {
		this.cursus = cursus;
	}

	public List<Cursus> getValidCursi() {
		return validCursi;
	}

	public void setValidCursi(List<Cursus> validCursi) {
		this.validCursi = validCursi;
	}

	public List<Cursus> getUnValidCursi() {
		return unValidCursi;
	}

	public void setUnValidCursi(List<Cursus> unValidCursi) {
		this.unValidCursi = unValidCursi;
	}

	public List<Stagiaire> getAllCandidates() {
		return allCandidates;
	}

	public void setAllCandidates(List<Stagiaire> allCandidates) {
		this.allCandidates = allCandidates;
	}
	
	
	

	

	

}
