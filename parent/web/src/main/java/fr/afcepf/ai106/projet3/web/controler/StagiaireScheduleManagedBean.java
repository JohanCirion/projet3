package fr.afcepf.ai106.projet3.web.controler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@ManagedBean (name="mbAccueilStagiaire")
@SessionScoped
public class StagiaireScheduleManagedBean {

	private List<Cours> courses;
	private Promotion promo;
	private ScheduleModel eventModel;
	private LazyScheduleModel lazyEventModel;
	private ScheduleEvent event = new DefaultScheduleEvent();
	
	@EJB
	private PromotionIBusiness proxyPromoBu;
	
	@ManagedProperty (value = "#{mbConnection.stagiaire}")
	private Stagiaire stagiaire;
	
	@PostConstruct
    public void init() {
		promo = proxyPromoBu.getPromotionByStagiaire(stagiaire);
		courses = proxyPromoBu.tousLesCours(promo);
		eventModel = new DefaultScheduleModel();
		for (Cours cours : courses) {
			eventModel.addEvent(makeEvent(cours));
		}
    }
	
	public DefaultScheduleEvent makeEvent(Cours c) {
    	Date dateFin = proxyPromoBu.calculDateFinCours(c);
    	DefaultScheduleEvent result = new DefaultScheduleEvent(c.getMatiere().getNom(), c.getDateDebut(), dateFin);
    	return result;
    }
	    
   public ScheduleModel getEventModel() {
       return eventModel;
   }
    
   public ScheduleModel getLazyEventModel() {
       return lazyEventModel;
   }

  
   public void addEvent() {
       if(event.getId() == null)
           eventModel.addEvent(event);
       else
           eventModel.updateEvent(event);
        
       event = new DefaultScheduleEvent();
   }
    
   public void onEventSelect(SelectEvent selectEvent) {
       event = (ScheduleEvent) selectEvent.getObject();
   }
    
   public void onDateSelect(SelectEvent selectEvent) {
       event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
   }
    
   public void onEventMove(ScheduleEntryMoveEvent event) {
       FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
        
       addMessage(message);
   }
    
   public void onEventResize(ScheduleEntryResizeEvent event) {
       FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
        
       addMessage(message);
   }
    
   private void addMessage(FacesMessage message) {
       FacesContext.getCurrentInstance().addMessage(null, message);
   }

	public List<Cours> getCourses() {
		return courses;
	}

	public void setCourses(List<Cours> courses) {
		this.courses = courses;
	}

	public Promotion getPromo() {
		return promo;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public Stagiaire getStagiaire() {
		return stagiaire;
	}

	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}
	
	
}
