package fr.afcepf.ai106.projet3.web.controler;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@ManagedBean (name = "mbPromo")
@SessionScoped
public class PromoManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Promotion promo = new Promotion();
	private List<Promotion> promos;
	private String msg = "";
	
	private Cursus cursus = new Cursus();
	private List<Cursus> cursi;
	
	@EJB
	private PromotionIBusiness proxyPromoBusiness;
	
	@EJB
	private CursusIBusiness proxyCursusBusiness;
	
	
	
	public String ajouter() {
		if(promo.getCursus() != null) {
			promo.setNumero(proxyPromoBusiness.calculNumPromo(promo.getCursus()));
			promo.setNom(promo.getCursus().getCode().toUpperCase() + " " + promo.getNumero());
			if(controlDate()) {
				promo = proxyPromoBusiness.ajouter(promo);
				promos = proxyPromoBusiness.getAllPromotions();
				return "/listePromos.xhtml?faces-redirect=true";
			} else {
				return msg;
				//return "/ajoutPromo.xhtml?faces-redirect=true";
			}
		}else {
			msg = "Veuillez sélectionner un cursus";
			return msg;
			//return "/ajoutPromo.xhtml?faces-redirect=true";
		}
		
	}
	
	public boolean controlDate() {
		boolean result = true;
		int day = promo.getDateDebut().getDay(); 
		if(day != 0 && day != 6) {
			Date debut = promo.getDateDebut();
			if(debut.before(new Date())) {
				msg = "Date invalide";
				result = false;
			}
		} else {
			msg = "Choisir un jour ouvré pour la date de début du cursus ";
			result = false;
		}
		return result;
	}
	
	public void preRender() {
		promo = new Promotion();
		cursi = proxyCursusBusiness.tousLesCursusValides();
	}
	
	public void postRender() {
		msg = "";
		promo = new Promotion();
	}
	
	@PostConstruct
	public void onInit() {
		promo = new Promotion();
		msg = "";
		promos = proxyPromoBusiness.getAllPromotions();
	}
	
	public Promotion getPromo() {
		return promo;
	}

	public List<Promotion> getPromos() {
		return promos;
	}

	public void setPromos(List<Promotion> promos) {
		this.promos = promos;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Cursus getCursus() {
		return cursus;
	}

	public void setCursus(Cursus cursus) {
		this.cursus = cursus;
	}

	public List<Cursus> getCursi() {
		return cursi;
	}

	public void setCursi(List<Cursus> cursi) {
		this.cursi = cursi;
	}
	

	

}
