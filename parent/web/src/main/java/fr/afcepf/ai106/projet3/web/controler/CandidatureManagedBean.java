package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiaireIBusiness;

@ManagedBean(name="mbCandidature")
@ViewScoped
public class CandidatureManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	List<Stagiaire> candidats;
	Stagiaire candidatSelected = new Stagiaire();
	List<ReunionInformation> listedesRI;
	ReunionInformation RISelected = new ReunionInformation();
	String compteur = "";
	String message = "";
	
	@EJB
	StagiaireIBusiness proxyStagiaireBusiness;
	
	@EJB
	EvenementIBusiness proxyEvenementBusiness;
	
	
	@PostConstruct
	public void init() {
		candidats = proxyStagiaireBusiness.getAllCandidats();
		listedesRI = proxyEvenementBusiness.getAllRI();
	}


	public void onPreRender() {
		candidatSelected = new Stagiaire();
		RISelected = new ReunionInformation();
		candidats = proxyStagiaireBusiness.getAllCandidats();
		listedesRI = proxyEvenementBusiness.getAllRI();
		if (candidats.size() == 0) {
			compteur = "Il n'y a plus de stagiaire en attente d'une invitation à une RI";
		} else {
			compteur = candidats.size() + " candidats en attente d'une invitation à une RI";
		}
	}
	
	
	public void inscrireUnStagiaireAUneRI() {
		proxyEvenementBusiness.inscrireStagiaireAUneRi(RISelected, candidatSelected);
		message = "Candidat invité";
	}
	
		
	
	public List<Stagiaire> getCandidats() {
		return candidats;
	}

	public void setCandidats(List<Stagiaire> candidats) {
		this.candidats = candidats;
	}

	public Stagiaire getCandidatSelected() {
		return candidatSelected;
	}

	public void setCandidatSelected(Stagiaire candidatSelected) {
		this.candidatSelected = candidatSelected;
	}

	public ReunionInformation getRISelected() {
		return RISelected;
	}

	public void setRISelected(ReunionInformation rISelected) {
		RISelected = rISelected;
	}

	public List<ReunionInformation> getListedesRI() {
		return listedesRI;
	}

	public void setListedesRI(List<ReunionInformation> listedesRI) {
		this.listedesRI = listedesRI;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public String getCompteur() {
		return compteur;
	}


	public void setCompteur(String compteur) {
		this.compteur = compteur;
	}
	
	
	
}
