package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;


import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.MatiereIBusiness;

@ManagedBean (name= "mbNouveauCursus")
@ViewScoped
public class NouveauCursusBean implements Serializable{
	
	
	@EJB
    private MatiereIBusiness proxyMatiereBusiness;

	@EJB
    private CursusIBusiness proxyCursusBusiness;
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Cursus nouveauCursus = new Cursus();
	private List<Matiere> matieres;
	private Matiere matiereAjoutee;
	private Integer dureeSelectionne;
	private List<CursusMatiere> matiereCursusDuCursus;
	private String message;
	private List<Cursus> cursi;
	private Integer id;
	private CursusMatiere cursusMatiereSelectionne;  
	
	@PostConstruct
	public void Init() { 
		matieres = proxyMatiereBusiness.getAll();
		cursi = proxyCursusBusiness.getAll();
		
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		id=0;
		if(params.get("id") != null) {
		id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
		nouveauCursus = proxyCursusBusiness.rechercher(id);

		matiereCursusDuCursus = proxyCursusBusiness.CursusMatiereDUnCursus(nouveauCursus);
		}
		
	}
	
	public String redirect(Integer id) {
		return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public String ajouterCursus() {
		nouveauCursus = proxyCursusBusiness.AjouterCursus(nouveauCursus);
		return "/CursusMatieres.xhtml?id=" + nouveauCursus.getId() + "&faces-redirect=true";
	}
	
	
	public String supprimer() {
		proxyCursusBusiness.supprimerMatiereDUnCursus(nouveauCursus, cursusMatiereSelectionne.getMatiere());
		return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public String monter() {
		proxyCursusBusiness.monterDansUnCursus(cursusMatiereSelectionne);
		return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public String descendre() {
		proxyCursusBusiness.descendreDansUnCursus(cursusMatiereSelectionne);
		return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	
	public String valider() {
		proxyCursusBusiness.validerUnCursus(nouveauCursus);
		return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public String invalider() {
		proxyCursusBusiness.invaliderUnCursus(nouveauCursus);
		return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public boolean visibiliteBoutonValider() {
		boolean visibilite = true;
		if (nouveauCursus.getDateValidation() != null) {
			visibilite = false;
		}
		
		return visibilite;
	}
	
	public boolean visibiliteBoutonInvalider() {
		boolean visibilite = true;
		if (nouveauCursus.getDateInvalid() != null || nouveauCursus.getDateValidation() == null) {
			visibilite = false;
		}
		
		return visibilite;
	}
	
	
	
	public String ajouter() {
	 proxyCursusBusiness.ajouterMatiereAUnCursus(nouveauCursus, matiereAjoutee, dureeSelectionne, matiereCursusDuCursus.size()+1);
	 return "/CursusMatieres.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	
	
	
	public Cursus getNouveauCursus() {
		return nouveauCursus;
	}
	public void setNouveauCursus(Cursus nouveauCursus) {
		this.nouveauCursus = nouveauCursus;
	}

	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	public List<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<Matiere> matieres) {
		this.matieres = matieres;
	}


	public List<Cursus> getCursi() {
		return cursi;
	}

	public void setCursi(List<Cursus> cursi) {
		this.cursi = cursi;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CursusMatiere> getMatiereCursusDuCursus() {
		return matiereCursusDuCursus;
	}

	public void setMatiereCursusDuCursus(List<CursusMatiere> matiereCursusDuCursus) {
		this.matiereCursusDuCursus = matiereCursusDuCursus;
	}

	public Integer getDureeSelectionne() {
		return dureeSelectionne;
	}

	public void setDureeSelectionne(Integer dureeSelectionne) {
		this.dureeSelectionne = dureeSelectionne;
	}

	public CursusMatiere getCursusMatiereSelectionne() {
		return cursusMatiereSelectionne;
	}

	public void setCursusMatiereSelectionne(CursusMatiere cursusMatiereSelectionne) {
		this.cursusMatiereSelectionne = cursusMatiereSelectionne;
	}

	public Matiere getMatiereAjoutee() {
		return matiereAjoutee;
	}

	public void setMatiereAjoutee(Matiere matiereAjoutee) {
		this.matiereAjoutee = matiereAjoutee;
	}
	
	
	
	

}
