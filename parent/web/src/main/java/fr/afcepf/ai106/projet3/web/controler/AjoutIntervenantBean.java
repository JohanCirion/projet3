package fr.afcepf.ai106.projet3.web.controler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.ibusiness.IntervenantIBusiness;

@ManagedBean (name="mbAjoutInterv")
@ViewScoped
public class AjoutIntervenantBean implements Serializable{


	private static final long serialVersionUID = 1L;

	@EJB
	private IntervenantIBusiness proxyIntervenantBusiness;


	private Intervenant interv = new Intervenant();
	private String message = "";
	private Part fichier;


	public void sauvegarderPhoto() {
		try (InputStream input = fichier.getInputStream()){
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String destination = servletContext.getRealPath("resources/IMG");
			System.out.println(destination);
			String nomFichier = fichier.getSubmittedFileName();

			Files.copy(input, new File(destination, nomFichier).toPath());
			interv.setPhoto(nomFichier);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String ajouterIntervenant() {
		proxyIntervenantBusiness.ajouter(interv);							
		return this.message = "Intervenant ajouté";
	}

	
	public void demo() {
		interv.setNom("Lambert");
		interv.setPrenom("Gérard");
		interv.setAdresse("12 rue de la voix cassée");
		interv.setCodePostal("94150");
		interv.setVille("Rungis");
		interv.setTel("06.37.19.82.64");
		interv.setMail("glambert@eql.fr");
	}
	

	public Intervenant getInterv() {
		return interv;
	}


	public void setInterv(Intervenant interv) {
		this.interv = interv;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Part getFichier() {
		return fichier;
	}


	public void setFichier(Part fichier) {
		this.fichier = fichier;
	}
	
	
	
}
