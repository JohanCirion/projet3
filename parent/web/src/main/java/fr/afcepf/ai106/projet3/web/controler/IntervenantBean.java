package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;



import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.CoursIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.IntervenantIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@ManagedBean (name="mbIntervenant")
@ViewScoped
public class IntervenantBean implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Intervenant interv;
	private List<Intervenant> intervenants; 

	private List<Cours> courses = new ArrayList<Cours>();
	private Cours cours;
	private Promotion promo;
	private List<Promotion> promotions;
	private Matiere matiere;
	private List<CursusMatiere> matieres = new ArrayList<CursusMatiere>();
	private CursusMatiere cursusmatiere;

	private String nomRecherche;
	private String message = "";

	@EJB
	private IntervenantIBusiness proxyIntervenantBusiness;
	

	@EJB
	private PromotionIBusiness proxyPromoBusiness;
	
	@EJB
	private CoursIBusiness proxyCoursBusiness;
	
	
	@PostConstruct
	public void init() {
		intervenants = proxyIntervenantBusiness.getAll();
		promotions = proxyPromoBusiness.getAllPromotions();		
	}
	
	public void rechargerIntervenant() {
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer id=0;
		if(params.get("id") != null) {
		id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
		interv = proxyIntervenantBusiness.rechercher(id);
		}
	}
	
	public void onPromoChange() {
		if(promo != null && !promo.getNom().equals("")) {
			matieres = proxyPromoBusiness.getMatieres(promo);
		} else {
			matieres = new ArrayList<>();
		}		
	}
			
	
	public String modifierIntervenant(){	
		proxyIntervenantBusiness.modifier(interv);		
		return this.message = "Intervenant modifié";	
	}
		
	
	public String afficherPromo() {
		promotions = proxyPromoBusiness.getAllPromotions();
		return "/AffecterIntervenant.xhtml?faces-redirect=true";	
	}
	
	
	public List<CursusMatiere> getPromoMatiere(Promotion p){
		return proxyPromoBusiness.getMatieres(p);				
	}
	
	public List<Cours> getCoursByPromoMatiere(Promotion p, Matiere m){
		return proxyCoursBusiness.getCoursByPromoMatiere(p, m);
	}
	
	public String affecterIntervenantMatiere() {		
		matiere = cursusmatiere.getMatiere();
		courses = proxyCoursBusiness.getCoursByPromoMatiere(promo, matiere);
		for (Cours cours: courses ) 
		{				
		proxyIntervenantBusiness.affecterIntervenantCours(cours, interv);
		}			
		return this.message = interv.getPrenom() + " " + interv.getNom() + " affecté(e) aux cours de " + matiere.getNom();									
	}

	
	
	
	public Intervenant getInterv() {
		return interv;
	}

	public void setInterv(Intervenant interv) {
		this.interv = interv;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<Intervenant> getIntervenants() {
		return intervenants;
	}


	public void setIntervenants(List<Intervenant> intervenants) {
		this.intervenants = intervenants;
	}


	public Cours getCours() {
		return cours;
	}


	public void setCours(Cours cours) {
		this.cours = cours;
	}



	public List<Cours> getCourses() {
		return courses;
	}


	public void setCourses(List<Cours> courses) {
		this.courses = courses;
	}



	public Promotion getPromo() {
		return promo;
	}



	public void setPromo(Promotion promo) {
		this.promo = promo;
	}



	public List<Promotion> getPromotions() {
		return promotions;
	}



	public void setPromotions(List<Promotion> promotions) {
		this.promotions = promotions;
	}


	public List<CursusMatiere> getMatieres() {
		return matieres;
	}


	public void setMatieres(List<CursusMatiere> matieres) {
		this.matieres = matieres;
	}


	public String getNomRecherche() {
		return nomRecherche;
	}


	public void setNomRecherche(String nomRecherche) {
		this.nomRecherche = nomRecherche;
	}


	public CursusMatiere getCursusmatiere() {
		return cursusmatiere;
	}


	public void setCursusmatiere(CursusMatiere cursusmatiere) {
		this.cursusmatiere = cursusmatiere;
	}

	


	
}
