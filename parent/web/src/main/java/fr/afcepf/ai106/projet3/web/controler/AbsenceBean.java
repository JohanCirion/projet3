package fr.afcepf.ai106.projet3.web.controler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai106.projet3.entities.Absence;
import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.ibusiness.AbsenceIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@ManagedBean (name="mbAbsence")
@ViewScoped
public class AbsenceBean {
	
	private Absence absence = new Absence();
	private Promotion promotion;
	private Stagiaire stagiaire;
	private List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();
	private List<Promotion> promotions;
	private List<Cours> cours = new ArrayList<Cours>();
	
		
	
	@EJB
	private AbsenceIBusiness proxyAbsenceBusiness;
	
	@EJB
	private PromotionIBusiness proxyPromotionBusiness;
	

	
	@PostConstruct
	public void init() { 
		promotions = proxyPromotionBusiness.getAllPromotions();
	}
	
	
	
    public void onPromoChange() {
		if(!promotion.equals(null) && !promotion.getNom().equals("")) {
			stagiaires = proxyPromotionBusiness.getStagiairesByPromo(promotion);
			cours = promotion.getCours();
			System.out.println(promotion.getCours());
			
		} else {
			stagiaires = new ArrayList<>();
		}	
    }
	
	
	

	public Promotion getPromotion() {
		return promotion;
	}
	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}
	public Stagiaire getStagiaire() {
		return stagiaire;
	}
	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}
	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}
	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}

	public List<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<Promotion> promotions) {
		this.promotions = promotions;
	}



	public Absence getAbsence() {
		return absence;
	}



	public void setAbsence(Absence absence) {
		this.absence = absence;
	}



	public List<Cours> getCours() {
		return cours;
	}



	public void setCours(List<Cours> cours) {
		this.cours = cours;
	}
	
	
	
	

	
	
}
