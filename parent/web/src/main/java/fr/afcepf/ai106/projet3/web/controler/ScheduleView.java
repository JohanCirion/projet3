package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ScheduleView implements Serializable{

    private static final long serialVersionUID = 1L;
    private List<Cours> courses;
	private List<CursusMatiere> matieres;
	private int idCours;
	
	

	@ManagedProperty (value = "#{mbListePromos.promo}")
	private Promotion promo;
	
	@ManagedProperty (value = "#{mbListePromos.idPromo}")
	private Integer idPromo;

	private ScheduleModel eventModel;
	private LazyScheduleModel lazyEventModel;
	private ScheduleEvent event = new DefaultScheduleEvent();
	
	@EJB
	private PromotionIBusiness proxyPromoBu;
 
    @PostConstruct
    public void init() {
		courses = proxyPromoBu.tousLesCours(promo);
		eventModel = new DefaultScheduleModel();
		for (Cours cours : courses) {
			eventModel.addEvent(makeEvent(cours));
		}
    }
    
    public DefaultScheduleEvent makeEvent(Cours c) {
    	Date dateFin = proxyPromoBu.calculDateFinCours(c);
    	Date dateDebut = c.getDateDebut();
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(dateDebut);
    	if(c.getMatin()) {
    		cal.set(Calendar.HOUR_OF_DAY, 8);
    	} else {
    		cal.set(Calendar.HOUR_OF_DAY, 13);
    	}
    	dateDebut = cal.getTime();
    	
    	DefaultScheduleEvent result = new DefaultScheduleEvent(nomCours(c), dateDebut, dateFin);
    	result.setId(c.getId().toString());
    	result.setDescription(c.getId().toString());
    	return result;
    }
    
    public void makeCours(ScheduleEvent event2) {
    	idCours = Integer.parseInt(event2.getDescription()) ;
    	Cours c = proxyPromoBu.getById(idCours);
    	c.setDateDebut(event2.getStartDate());
    	c = proxyPromoBu.updateCours(c);
    }
    
    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);
         
        return calendar.getTime();
    }
    
    public String nomCours(Cours c) {
    	String result = c.getMatiere().getNom();
    	if (c.getIntervenant() != null) {
    		result += " - " + c.getIntervenant().getNom();
    	}
    	return result;
    }
     
    private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
 
        return calendar;
    }
     
    public void addEvent() {
        if(event.getId() == null)
            eventModel.addEvent(event);
        else {
        	eventModel.updateEvent(event);
        	makeCours(event);
        }
        
         
        event = new DefaultScheduleEvent();
    }
     
    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }
     
    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
    }
     
    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
         
        addMessage(message);
    }
     
    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
         
        addMessage(message);
    }
     
    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public ScheduleEvent getEvent() {
        return event;
    }
 
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
    
    public ScheduleModel getEventModel() {
        return eventModel;
    }
     
    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }
    
    public List<Cours> getCourses() {
		return courses;
	}

	public void setCourses(List<Cours> courses) {
		this.courses = courses;
	}

	public List<CursusMatiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<CursusMatiere> matieres) {
		this.matieres = matieres;
	}

	public Promotion getPromo() {
		return promo;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	public Integer getIdPromo() {
		return idPromo;
	}

	public void setIdPromo(Integer idPromo) {
		this.idPromo = idPromo;
	}
	
	public int getIdCours() {
		return idCours;
	}

	public void setIdCours(int idCours) {
		this.idCours = idCours;
	}
	
	
	
}
