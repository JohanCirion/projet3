package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.management.relation.Role;

import fr.afcepf.ai106.projet3.entities.Administratif;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Profil;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;
import fr.afcepf.ai106.projet3.ibusiness.UtilisateurIBusiness;

@ManagedBean (name="mbIdentifiants")
@ViewScoped
public class IdentifiantsManagedBean implements Serializable {


	private static final long serialVersionUID = 1L;

	private List<Profil> profils;
	private String name;

	private Utilisateur utilisateur;
	private List<Stagiaire> stagiaires;
	private List<Intervenant> intervenants;
	private List<Administratif> administrateurs;
	private Stagiaire stagiaireSelected;
	
	private Profil profil;
	private String login;
	private String password;
	
	private String message;
	private String messagebis;


	@EJB
	private UtilisateurIBusiness proxyUtilisateurBusiness;


	@PostConstruct
	public void init() {
		profils = proxyUtilisateurBusiness.getAllProfil();
	}


	public void afficherPersonnes(){
		switch(profil.getNom()) {
		case("Stagiaire") :
			stagiaires = proxyUtilisateurBusiness.getStagiaireByName(name);
		case("Intervenant") :
			intervenants = proxyUtilisateurBusiness.getIntervenantByName(name);
		case("Administratif") :
			administrateurs = proxyUtilisateurBusiness.getAdministratifByName(name);
		}

	}

	public Stagiaire selectCeStagiaire(Stagiaire stag) {
		stagiaireSelected = stag;
		messagebis = "Veuillez choisir un login et un mot de passe";
		return stagiaireSelected;
	}

	
	public void enregistrerLoginPasswordStag() {
		System.out.println(stagiaireSelected);
		utilisateur = new Utilisateur();
		
		utilisateur.setProfil(profil);
		utilisateur.setLogin(login);
		utilisateur.setPassword(password);
		proxyUtilisateurBusiness.createLoginAndPasswordforStagiaire(utilisateur, stagiaireSelected);
		message = "Login et mot de passe créés";
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}


	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}


	public List<Intervenant> getIntervenants() {
		return intervenants;
	}


	public void setIntervenants(List<Intervenant> intervenants) {
		this.intervenants = intervenants;
	}


	public List<Administratif> getAdministrateurs() {
		return administrateurs;
	}


	public void setAdministrateurs(List<Administratif> administrateurs) {
		this.administrateurs = administrateurs;
	}


	public Stagiaire getStagiaireSelected() {
		return stagiaireSelected;
	}

	public void setStagiaireSelected(Stagiaire stagiaireSelected) {
		this.stagiaireSelected = stagiaireSelected;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public List<Profil> getProfils() {
		return profils;
	}


	public void setProfils(List<Profil> profils) {
		this.profils = profils;
	}


	public String getMessagebis() {
		return messagebis;
	}


	public void setMessagebis(String messagebis) {
		this.messagebis = messagebis;
	}
	
	
	
	
}
