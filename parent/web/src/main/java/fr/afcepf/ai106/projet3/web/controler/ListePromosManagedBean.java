package fr.afcepf.ai106.projet3.web.controler;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@ManagedBean (name = "mbListePromos")
@SessionScoped
public class ListePromosManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Promotion promo = new Promotion();
	private List<Promotion> promos;
	private Integer idPromo;

	@EJB
	private PromotionIBusiness proxyPromoBusiness;
	
	@EJB
	private CursusIBusiness proxyCursusBusiness;
	
	@PostConstruct
	public void onInit() {
		promos = proxyPromoBusiness.getAllPromotions();
		System.out.println(promos.size());
	}
	
	public void preRender() {
		promos = proxyPromoBusiness.getAllPromotions();
	}
	
	public String rafraichir() {
		promos = proxyPromoBusiness.getAllPromotions();
		return "/listePromos.xhtml?faces-redirect=true";
	}
	
    public String afficherPromo() {
    	idPromo = promo.getId();
    	return "/scheduleView.xhtml?faces-redirect=true";
    }

	public Promotion getPromo() {
		return promo;
	}

	public List<Promotion> getPromos() {
		return promos;
	}

	public void setPromos(List<Promotion> promos) {
		this.promos = promos;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	public Integer getIdPromo() {
		return idPromo;
	}

	public void setIdPromo(Integer idPromo) {
		this.idPromo = idPromo;
	}

	
}
