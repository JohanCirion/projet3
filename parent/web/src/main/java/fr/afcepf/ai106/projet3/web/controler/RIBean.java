package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Salle;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.LogistiqueIBusiness;

@ManagedBean (name= "mbRI")
@ViewScoped
public class RIBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private ReunionInformation ri; 
	private String message = "";
	private Cursus cursus;
	private Date date;
	private List<Cursus> cursi;
	private Salle salle;
	private List<Salle> salles;
	private List<ReunionInformation> listeRIs;
	
	@EJB
    private EvenementIBusiness proxyEvenenementBusiness;

	@EJB
    private CursusIBusiness proxyCursusBusiness;
	
	@EJB
    private LogistiqueIBusiness proxyLogistiqueBusiness;
	
	@PostConstruct
	public void Init() {
		cursi = proxyCursusBusiness.getAll();
		salles = proxyLogistiqueBusiness.getAllSalle();
		listeRIs = proxyEvenenementBusiness.getAllRIFutur();
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer id=0;
		if(params.get("id") != null) {
		id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
			ri=proxyEvenenementBusiness.rechercherRIParId(id);
		}	
	}
	
	public String redirect(Integer id) {
		return "/ModifierRI.xhtml?id=" + id + "&faces-redirect=true";
	}
	
	public void AjouterRI() {
		ri = new ReunionInformation();
		ri.setCursus(cursus);
		ri.setDate(date);
		ri.setSalle(salle);
		ri =proxyEvenenementBusiness.AjouterRI(ri);
		message = "Réunion d'information programmée";
//		return "/RIAjoutee.xhtml?faces-redirect=true";			
	}
	
	public String modifierRI() {
		ri =proxyEvenenementBusiness.modifierRI(ri);
		message = "Réunion d'Information modifiée";
		return "/RIModifiee.xhtml?faces-redirect=true";			
	}

	public ReunionInformation getRi() {
		return ri;
	}

	public void setRi(ReunionInformation ri) {
		this.ri = ri;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Cursus getCursus() {
		return cursus;
	}

	public void setCursus(Cursus cursus) {
		this.cursus = cursus;
	}

	public List<Cursus> getCursi() {
		return cursi;
	}

	public void setCursi(List<Cursus> cursi) {
		this.cursi = cursi;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public List<Salle> getSalles() {
		return salles;
	}

	public void setSalles(List<Salle> salles) {
		this.salles = salles;
	}

	public List<ReunionInformation> getListeRIs() {
		return listeRIs;
	}

	public void setListeRIs(List<ReunionInformation> listeRIs) {
		this.listeRIs = listeRIs;
	}

	
}
