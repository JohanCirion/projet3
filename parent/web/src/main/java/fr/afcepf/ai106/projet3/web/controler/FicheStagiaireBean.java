package fr.afcepf.ai106.projet3.web.controler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import javax.ws.rs.core.StreamingOutput;

import org.primefaces.model.UploadedFile;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Diplome;
import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Entreprise;
import fr.afcepf.ai106.projet3.entities.Entretien;
import fr.afcepf.ai106.projet3.entities.Financement;
import fr.afcepf.ai106.projet3.entities.Indemnisation;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;
import fr.afcepf.ai106.projet3.entities.StatutProfessionnel;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.entities.Titre;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiaireIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiairePromoIBusiness;


@ManagedBean(name="mbFicheStagiaire")
@SessionScoped
public class FicheStagiaireBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EJB
	private StagiaireIBusiness proxyStagiaireBusiness;	
	
	@EJB
	private PromotionIBusiness proxyPromotionBusiness;
	
	@EJB
	private StagiairePromoIBusiness proxyStagiairePromoBusiness;
	
	@EJB
	private EvenementIBusiness proxyEvenementBusiness;
	

	private String messageFiche;
	
	private List<Stagiaire> stagiaires;
	
	private Stagiaire stagiaire = new Stagiaire();
	
	private List<Promotion> listPromotions;
	private Promotion promotion;
	
	private List<Titre> listTitres;
	private Titre titre;

	private List<Cursus> listCursus;
	private Cursus cursusVise;
	
	private List<Provenance> listProvenance;
	private Provenance provenance;;
	
	private List<Diplome> listDiplomes;
	private Diplome diplome;
	
	private List<Dispositif> listDispositifs;
	private Dispositif dispositif;
	
	private List<Test> listTest;
	private Test test = new Test();
	
	private List<Financement> listFinancements;
	private Financement financement;

	private List<Entreprise> listEntreprises;
	private Entreprise entreprise;

	private List<StatutProfessionnel> listStatutPro;
	private StatutProfessionnel statutpro;

	private List<Indemnisation> listIndemnisations;
	private Indemnisation indemnisation;
	
	private List<Entretien> listEntretien;
	private Entretien entretien = new Entretien();
	
	private Integer id;
	
	private Boolean accept;

	private StagiaireTest st = new StagiaireTest();
	
	private Part fichier;
	private Part cv;
	
	

	@PostConstruct
	public void Init() {
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer id=0;
		if(params.get("id") != null) {
			id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
			stagiaire = proxyStagiaireBusiness.rechercher(id);
		}
		listTitres = proxyStagiaireBusiness.getAllTitres();
		listCursus = proxyStagiaireBusiness.getAllCursus();
		listProvenance = proxyStagiaireBusiness.getAllProvenances();
		listDiplomes = proxyStagiaireBusiness.getAllDiplomes();
		listPromotions = proxyPromotionBusiness.getAllFuturesPromotions();		
		listDispositifs = proxyStagiaireBusiness.getAllDispositifs();
		listTest = proxyEvenementBusiness.getAllTests();		
		listFinancements = proxyStagiaireBusiness.getAllFinancements();
		listEntreprises = proxyStagiaireBusiness.getAllEntreprises();
		listStatutPro = proxyStagiaireBusiness.getAllStatutPro();
		listIndemnisations = proxyStagiaireBusiness.getAllIndemnisations();
		listEntretien = proxyStagiaireBusiness.getAllEntretien();	
		st = proxyEvenementBusiness.getStagiaireTestParStagiaire(stagiaire);	
		test = proxyEvenementBusiness.getTestByStagiaire(stagiaire);
		promotion = new Promotion();
		promotion = proxyPromotionBusiness.getPromotionByStagiaire(stagiaire);
		dispositif = new Dispositif();
		dispositif = proxyStagiaireBusiness.getDispositifByStagiaire(stagiaire);
		entretien = new Entretien();
		entretien= proxyStagiaireBusiness.getEntretienParStagiaire(stagiaire);
		accept = entretien.getAcceptation();	
	}
	
	
	
	public void rechargerStagiaire() {
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		id=0;
		if(params.get("id") != null) {
			id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
			stagiaire = proxyStagiaireBusiness.rechercher(id);
			test = proxyEvenementBusiness.getTestByStagiaire(stagiaire);
			promotion = new Promotion();
			promotion = proxyPromotionBusiness.getPromotionByStagiaire(stagiaire);
			dispositif = new Dispositif();
			dispositif = proxyStagiaireBusiness.getDispositifByStagiaire(stagiaire);
			st = proxyEvenementBusiness.getStagiaireTestParStagiaire(stagiaire);
			entretien = new Entretien();
			entretien= proxyStagiaireBusiness.getEntretienParStagiaire(stagiaire);
			listTitres = proxyStagiaireBusiness.getAllTitres();
			listCursus = proxyStagiaireBusiness.getAllCursus();
			listProvenance = proxyStagiaireBusiness.getAllProvenances();
			listDiplomes = proxyStagiaireBusiness.getAllDiplomes();
			listPromotions = proxyPromotionBusiness.getAllFuturesPromotions();		
			listDispositifs = proxyStagiaireBusiness.getAllDispositifs();
			listTest = proxyEvenementBusiness.getAllTests();		
			listFinancements = proxyStagiaireBusiness.getAllFinancements();
			listEntreprises = proxyStagiaireBusiness.getAllEntreprises();
			listStatutPro = proxyStagiaireBusiness.getAllStatutPro();
			listIndemnisations = proxyStagiaireBusiness.getAllIndemnisations();
			listEntretien = proxyStagiaireBusiness.getAllEntretien();
			accept = entretien.getAcceptation();
		}
	}

	
	
	public String enregister() {
			proxyStagiaireBusiness.modifierUnStagiaire(stagiaire);
			
			if(!test.getDateTest().equals(null) || !test.getDateTest().equals(st.getTest().getDateTest())) {
			st.setStagiaire(stagiaire);
			st.setTest(test);
			st = proxyStagiaireBusiness.EnregistrerUnStagiaireTest(st);
			}
			entretien.setAcceptation(accept);
			proxyStagiaireBusiness.enregistrerUnStagiaireAUnEntretien(stagiaire, entretien);

			if(dispositif.getId() != null && promotion.getId() != null) {
			
			StagiairePromo sp = new StagiairePromo(dispositif, promotion, stagiaire);
			proxyStagiairePromoBusiness.ajouterStagPromo(sp);
			}
			messageFiche = "Modification(s) enregistrée(s)";

		return "/FicheStagiaire.xhtml?id=" + stagiaire.getId() +"&faces-redirect=true";

	}


	public String sauvegarderPhoto() {
		try (InputStream input = fichier.getInputStream()){
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String destination = servletContext.getRealPath("resources/IMG");
			System.out.println(destination);
			String nomFichier = fichier.getSubmittedFileName();

			Files.copy(input, new File(destination, nomFichier).toPath());
			stagiaire.setPhoto(nomFichier);
			proxyStagiaireBusiness.modifierUnStagiaire(stagiaire);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "/FicheStagiaire.xhtml?id=" + stagiaire.getId() +"&faces-redirect=true";
	}
	
	
	
	public String sauvegarderCV() {
		try (InputStream input2 = cv.getInputStream()){
			ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String destination2 = servletContext.getRealPath("resources/CV");
			System.out.println(destination2);
			String nomFichier2 = cv.getSubmittedFileName();

			Files.copy(input2, new File(destination2, nomFichier2).toPath());
			stagiaire.setCv(nomFichier2);
			proxyStagiaireBusiness.modifierUnStagiaire(stagiaire);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "/FicheStagiaire.xhtml?id=" + stagiaire.getId() +"&faces-redirect=true";
	}
	

	
	public void demo1() {
			stagiaire.setAdresse("31 rue des Vignes");
			stagiaire.setCodePostal("75014");
			stagiaire.setVille("Paris");
			stagiaire.setTel("06.74.85.96.31");
			@SuppressWarnings("deprecation")
			Date birth = new Date(1985, 05, 05);
			stagiaire.setDateNaissance(birth);
			stagiaire.setLieuNaissance("La Rochelle");
			stagiaire.setPaysNaissance("France");
			stagiaire.setExpPro("Juriste");
			stagiaire.setDureeExpPro("7 an");
			Provenance prov = listProvenance.get(0);
			stagiaire.setProvenance(prov);
			Diplome dip = listDiplomes.get(2);
			stagiaire.setDiplome(dip);
	}
	
	
	public void demo2() {
		Dispositif disp = listDispositifs.get(1);
//		StagiairePromo stgpr = new StagiairePromo(disp, stagiaire);
		setDispositif(disp);
		Financement fin = listFinancements.get(3);
		stagiaire.setFinancement(fin);
		Entreprise ent = listEntreprises.get(2);
		stagiaire.setEntreprise(ent);
		StatutProfessionnel statu = listStatutPro.get(0);
		stagiaire.setStatut(statu);
		stagiaire.setIdPoleEmp("6482735M");
		@SuppressWarnings("deprecation")
		Date datePE = new Date(05, 06, 07);
		stagiaire.setDatePoleEmp(datePE);
		Indemnisation inde = listIndemnisations.get(0);
		stagiaire.setIndemnisation(inde);
	}
	
	

	public String getMessageFiche() {
		return messageFiche;
	}




	public void setMessageFiche(String messageFiche) {
		this.messageFiche = messageFiche;
	}




	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}




	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}




	public Stagiaire getStagiaire() {
		return stagiaire;
	}




	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}




	public List<Promotion> getListPromotions() {
		return listPromotions;
	}




	public void setListPromotions(List<Promotion> listPromotions) {
		this.listPromotions = listPromotions;
	}




	public Promotion getPromotion() {
		return promotion;
	}




	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}




	public List<Titre> getListTitres() {
		return listTitres;
	}




	public void setListTitres(List<Titre> listTitres) {
		this.listTitres = listTitres;
	}




	public Titre getTitre() {
		return titre;
	}




	public void setTitre(Titre titre) {
		this.titre = titre;
	}




	public List<Cursus> getListCursus() {
		return listCursus;
	}




	public void setListCursus(List<Cursus> listCursus) {
		this.listCursus = listCursus;
	}




	public Cursus getCursusVise() {
		return cursusVise;
	}




	public void setCursusVise(Cursus cursusVise) {
		this.cursusVise = cursusVise;
	}




	public List<Provenance> getListProvenance() {
		return listProvenance;
	}




	public void setListProvenance(List<Provenance> listProvenance) {
		this.listProvenance = listProvenance;
	}




	public Provenance getProvenance() {
		return provenance;
	}




	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}




	public List<Diplome> getListDiplomes() {
		return listDiplomes;
	}




	public void setListDiplomes(List<Diplome> listDiplomes) {
		this.listDiplomes = listDiplomes;
	}




	public Diplome getDiplome() {
		return diplome;
	}




	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}




	public List<Dispositif> getListDispositifs() {
		return listDispositifs;
	}




	public void setListDispositifs(List<Dispositif> listDispositifs) {
		this.listDispositifs = listDispositifs;
	}




	public Dispositif getDispositif() {
		return dispositif;
	}




	public void setDispositif(Dispositif dispositif) {
		this.dispositif = dispositif;
	}




	public List<Test> getListTest() {
		return listTest;
	}




	public void setListTest(List<Test> listTest) {
		this.listTest = listTest;
	}




	public Test getTest() {
		return test;
	}




	public void setTest(Test test) {
		this.test = test;
	}




	public List<Financement> getListFinancements() {
		return listFinancements;
	}




	public void setListFinancements(List<Financement> listFinancements) {
		this.listFinancements = listFinancements;
	}




	public Financement getFinancement() {
		return financement;
	}




	public void setFinancement(Financement financement) {
		this.financement = financement;
	}




	public List<Entreprise> getListEntreprises() {
		return listEntreprises;
	}




	public void setListEntreprises(List<Entreprise> listEntreprises) {
		this.listEntreprises = listEntreprises;
	}




	public Entreprise getEntreprise() {
		return entreprise;
	}




	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}




	public List<StatutProfessionnel> getListStatutPro() {
		return listStatutPro;
	}




	public void setListStatutPro(List<StatutProfessionnel> listStatutPro) {
		this.listStatutPro = listStatutPro;
	}




	public StatutProfessionnel getStatutpro() {
		return statutpro;
	}




	public void setStatutpro(StatutProfessionnel statutpro) {
		this.statutpro = statutpro;
	}




	public List<Indemnisation> getListIndemnisations() {
		return listIndemnisations;
	}




	public void setListIndemnisations(List<Indemnisation> listIndemnisations) {
		this.listIndemnisations = listIndemnisations;
	}




	public Indemnisation getIndemnisation() {
		return indemnisation;
	}




	public void setIndemnisation(Indemnisation indemnisation) {
		this.indemnisation = indemnisation;
	}




	public List<Entretien> getListEntretien() {
		return listEntretien;
	}




	public void setListEntretien(List<Entretien> listEntretien) {
		this.listEntretien = listEntretien;
	}




	public Entretien getEntretien() {
		return entretien;
	}




	public void setEntretien(Entretien entretien) {
		this.entretien = entretien;
	}




	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}


	public Boolean getAccept() {
		return accept;
	}


	public void setAccept(Boolean accept) {
		this.accept = accept;
	}


	public StagiaireTest getSt() {
		return st;
	}


	public void setSt(StagiaireTest st) {
		this.st = st;
	}


	public Part getFichier() {
		return fichier;
	}


	public void setFichier(Part fichier) {
		this.fichier = fichier;
	}


	public Part getCv() {
		return cv;
	}


	public void setCv(Part cv) {
		this.cv = cv;
	}

	
	
	
	
	}
