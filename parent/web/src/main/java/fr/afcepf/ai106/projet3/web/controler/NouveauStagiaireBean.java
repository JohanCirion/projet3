package fr.afcepf.ai106.projet3.web.controler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Diplome;
import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Entreprise;
import fr.afcepf.ai106.projet3.entities.Entretien;
import fr.afcepf.ai106.projet3.entities.Financement;
import fr.afcepf.ai106.projet3.entities.Indemnisation;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StatutProfessionnel;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.entities.Titre;
import fr.afcepf.ai106.projet3.ibusiness.CursusIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiaireIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiairePromoIBusiness;

@ManagedBean (name="mbNouveauStagiaire")
@ViewScoped
public class NouveauStagiaireBean {

	
	private static final long serialVersionUID = 1L;

	@EJB
	private StagiaireIBusiness proxyStagiaireBusiness;	
	
	@EJB
	private PromotionIBusiness proxyPromotionBusiness;
	
	@EJB
	private StagiairePromoIBusiness proxyStagiairePromoBusiness;
	
	@EJB
	private EvenementIBusiness proxyEvenementBusiness;
	
	@EJB
	private CursusIBusiness proxyCursusBusiness;

	private String messageFiche;
	
	private List<Stagiaire> stagiaires;
	
	private Stagiaire stagiaire = new Stagiaire();
	
	private List<Promotion> listPromotions;
	private Promotion promotion;
	
	private List<Titre> listTitres;
	private Titre titre;

	private List<Cursus> listCursus;
	private Cursus cursusVise;
	
	private List<Provenance> listProvenance;
	private Provenance provenance;;
	
	private List<Diplome> listDiplomes;
	private Diplome diplome;
	
	private List<Dispositif> listDispositifs;
	private Dispositif dispositif;
	
	private List<Test> listTest;
	private Test test;
	
	private List<Financement> listFinancements;
	private Financement financement;

	private List<Entreprise> listEntreprises;
	private Entreprise entreprise;

	private List<StatutProfessionnel> listStatutPro;
	private StatutProfessionnel statutpro;

	private List<Indemnisation> listIndemnisations;
	private Indemnisation indemnisation;
	
	private List<Entretien> listEntretien;
	private Entretien entretien;
	
	private Integer id;
	
	private Part cv;

	
	@PostConstruct
	public void Init() {
		listTitres = proxyStagiaireBusiness.getAllTitres();
		listCursus = proxyCursusBusiness.tousLesCursusValides();
		listProvenance = proxyStagiaireBusiness.getAllProvenances();
		listDiplomes = proxyStagiaireBusiness.getAllDiplomes();
		listPromotions = proxyPromotionBusiness.getAllFuturesPromotions();		
		listDispositifs = proxyStagiaireBusiness.getAllDispositifs();
		listTest = proxyEvenementBusiness.getAllTests();		
		listFinancements = proxyStagiaireBusiness.getAllFinancements();
		listEntreprises = proxyStagiaireBusiness.getAllEntreprises();
		listStatutPro = proxyStagiaireBusiness.getAllStatutPro();
		listIndemnisations = proxyStagiaireBusiness.getAllIndemnisations();
	}

	public void enregistrer() {
		if(stagiaire.getId() == null) {
		
		stagiaire = proxyStagiaireBusiness.enregistrerUnStagiaire(stagiaire);
		messageFiche = "Stagiaire créé";
		} else {
			messageFiche = "Stagiaire déjà créé";
		}
		
	}
	
	public void enregistrerFormulaire() {
		if(stagiaire.getId() == null) {
			try (InputStream input2 = cv.getInputStream()){
				
				ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
				String destination2 = servletContext.getRealPath("resources/CV");
				String nomFichier2 = cv.getSubmittedFileName();

				Files.copy(input2, new File(destination2, nomFichier2).toPath());
				stagiaire.setCv(nomFichier2);
				} catch (IOException e) {
					e.printStackTrace();
				}
		stagiaire.setPhoto("nophoto.jpg");
		stagiaire = proxyStagiaireBusiness.enregistrerUnStagiaire(stagiaire);
		messageFiche = "Candidature enregistrée";
		} 
		
	}
	
	
	public void demoFormulaire() {
		Titre tit = listTitres.get(1);
		stagiaire.setTitre(tit);
		stagiaire.setNom("Rachmaninov");
		stagiaire.setPrenom("Ada");
		stagiaire.setMail("adarachmaninov@gmail.com");
		Cursus curvis = listCursus.get(0);
		stagiaire.setCursusVise(curvis);
	}
	
		
	public String getMessageFiche() {
		return messageFiche;
	}


	public void setMessageFiche(String messageFiche) {
		this.messageFiche = messageFiche;
	}


	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}


	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}


	public Stagiaire getStagiaire() {
		return stagiaire;
	}


	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}


	public List<Promotion> getListPromotions() {
		return listPromotions;
	}


	public void setListPromotions(List<Promotion> listPromotions) {
		this.listPromotions = listPromotions;
	}


	public Promotion getPromotion() {
		return promotion;
	}


	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}


	public List<Titre> getListTitres() {
		return listTitres;
	}


	public void setListTitres(List<Titre> listTitres) {
		this.listTitres = listTitres;
	}


	public Titre getTitre() {
		return titre;
	}


	public void setTitre(Titre titre) {
		this.titre = titre;
	}


	public List<Cursus> getListCursus() {
		return listCursus;
	}


	public void setListCursus(List<Cursus> listCursus) {
		this.listCursus = listCursus;
	}


	public Cursus getCursusVise() {
		return cursusVise;
	}


	public void setCursusVise(Cursus cursusVise) {
		this.cursusVise = cursusVise;
	}


	public List<Provenance> getListProvenance() {
		return listProvenance;
	}


	public void setListProvenance(List<Provenance> listProvenance) {
		this.listProvenance = listProvenance;
	}


	public Provenance getProvenance() {
		return provenance;
	}


	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}


	public List<Diplome> getListDiplomes() {
		return listDiplomes;
	}


	public void setListDiplomes(List<Diplome> listDiplomes) {
		this.listDiplomes = listDiplomes;
	}


	public Diplome getDiplome() {
		return diplome;
	}


	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}


	public List<Dispositif> getListDispositifs() {
		return listDispositifs;
	}


	public void setListDispositifs(List<Dispositif> listDispositifs) {
		this.listDispositifs = listDispositifs;
	}


	public Dispositif getDispositif() {
		return dispositif;
	}


	public void setDispositif(Dispositif dispositif) {
		this.dispositif = dispositif;
	}


	public List<Test> getListTest() {
		return listTest;
	}


	public void setListTest(List<Test> listTest) {
		this.listTest = listTest;
	}


	public Test getTest() {
		return test;
	}


	public void setTest(Test test) {
		this.test = test;
	}


	public List<Financement> getListFinancements() {
		return listFinancements;
	}


	public void setListFinancements(List<Financement> listFinancements) {
		this.listFinancements = listFinancements;
	}


	public Financement getFinancement() {
		return financement;
	}


	public void setFinancement(Financement financement) {
		this.financement = financement;
	}


	public List<Entreprise> getListEntreprises() {
		return listEntreprises;
	}


	public void setListEntreprises(List<Entreprise> listEntreprises) {
		this.listEntreprises = listEntreprises;
	}


	public Entreprise getEntreprise() {
		return entreprise;
	}


	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}


	public List<StatutProfessionnel> getListStatutPro() {
		return listStatutPro;
	}


	public void setListStatutPro(List<StatutProfessionnel> listStatutPro) {
		this.listStatutPro = listStatutPro;
	}


	public StatutProfessionnel getStatutpro() {
		return statutpro;
	}


	public void setStatutpro(StatutProfessionnel statutpro) {
		this.statutpro = statutpro;
	}


	public List<Indemnisation> getListIndemnisations() {
		return listIndemnisations;
	}


	public void setListIndemnisations(List<Indemnisation> listIndemnisations) {
		this.listIndemnisations = listIndemnisations;
	}


	public Indemnisation getIndemnisation() {
		return indemnisation;
	}


	public void setIndemnisation(Indemnisation indemnisation) {
		this.indemnisation = indemnisation;
	}


	public List<Entretien> getListEntretien() {
		return listEntretien;
	}


	public void setListEntretien(List<Entretien> listEntretien) {
		this.listEntretien = listEntretien;
	}


	public Entretien getEntretien() {
		return entretien;
	}


	public void setEntretien(Entretien entretien) {
		this.entretien = entretien;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public Part getCv() {
		return cv;
	}

	public void setCv(Part cv) {
		this.cv = cv;
	}
	
	
	
}
