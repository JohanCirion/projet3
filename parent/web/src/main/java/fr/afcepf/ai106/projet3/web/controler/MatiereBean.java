package fr.afcepf.ai106.projet3.web.controler;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.ibusiness.MatiereIBusiness;

@ManagedBean (name= "mbMatiere")
@ViewScoped
public class MatiereBean {
	
	private Matiere matiere = new Matiere();
	private List<Matiere> matieres;
	private String message = "";
	Integer id;
	
	public String redirect(Integer id) {
		return "ModifierMatiere.xhtml?id=" + id + "&faces-redirect=true";
	}

	
	@EJB
    private MatiereIBusiness proxyMatiereBusiness;
	
	@PostConstruct
	public void Init() {
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		id=0;
		if(params.get("id") != null) {
		id = Integer.parseInt(params.get("id"));
		}
		matieres = proxyMatiereBusiness.getAll();
		if(id!=0) {
		matiere = proxyMatiereBusiness.rechercher(id);
		}
		this.message = "";
	}
	
	public void rechargerMatiere() {
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

		id = Integer.parseInt(params.get("id"));

		matiere = proxyMatiereBusiness.rechercher(id);

	}
	public Matiere ajouterMatiere(Matiere m) {
		m = proxyMatiereBusiness.ajouter(m);
		this.message = "Matière ajoutée";
		return m;
	}
	
	public String modifierMatiere() {
		matiere = proxyMatiereBusiness.modifier(matiere);
		this.message = "Matière modifiée";
		return "MatiereModifiee.xhtml?faces-redirect=true";
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public List<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<Matiere> matieres) {
		this.matieres = matieres;
	}


	
	
	
	

}
