package fr.afcepf.ai106.projet3.web.controler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Diplome;
import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Entreprise;
import fr.afcepf.ai106.projet3.entities.Financement;
import fr.afcepf.ai106.projet3.entities.Indemnisation;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StatutProfessionnel;
import fr.afcepf.ai106.projet3.entities.Test;
import fr.afcepf.ai106.projet3.entities.Titre;
import fr.afcepf.ai106.projet3.ibusiness.EvenementIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;
import fr.afcepf.ai106.projet3.ibusiness.StagiaireIBusiness;

@ManagedBean(name="mbRechercheStagiaire")
@ViewScoped
public class RechercheStagiaireBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private StagiaireIBusiness proxyStagiaireBusiness;	

	@EJB
	private PromotionIBusiness proxyPromotionBusiness;

	@EJB
	private EvenementIBusiness proxyEvenementBusiness;


	private String nomRecherche;	
	private String messageRecherche;

	
	private List<Stagiaire> stagiaires;
	
	
	private Stagiaire stagiaire = new Stagiaire();

	private List<Promotion> listPromotions;
	private Promotion promotion;



	private Titre titre = new Titre();
	private Cursus cursusVise = new Cursus();
	private Provenance provenance;
	private Diplome diplome;
	private Dispositif dispositif;
	private Test test;
	private Financement financement;	
	private Entreprise entreprise;	
	private StatutProfessionnel statutpro;
	private Indemnisation indemnisation;



	@PostConstruct
	public void Init() {
		listPromotions = proxyPromotionBusiness.getAllPromotions();
	
	}



	public void clean() {
//		stagiaire = new Stagiaire();
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
//        session.invalidate();
		System.out.println("CLEANNNNNNN");
	}
	public void rechercherUnStagiaire(){
		stagiaires = proxyStagiaireBusiness.rechercherUnStagiaire(nomRecherche);
		if (stagiaires.size() == 0) {
			messageRecherche = "Aucun stagiaire trouvé";
			stagiaires = new ArrayList<Stagiaire>();
		} else {
			messageRecherche = "Résultat de la recherche : " + stagiaires.size() + " stagiaire(s) trouvé(s)";
		}
		
	}


	public void rechercherParPromo() {
		stagiaires = proxyPromotionBusiness.getStagiairesByPromo(promotion);
		
		if (stagiaires.size() == 0) {
			messageRecherche = "Aucun stagiaire trouvé dans cette promotion";
			stagiaires = new ArrayList<Stagiaire>();
		} else {
			messageRecherche = "Résultat de la recherche : " + stagiaires.size() + " stagiaire(s) trouvé(s) dans cette promotion";
		}
		
//		return  "/ListeStagiaires.xhtml?faces-redirect=true";
	}
	

	public void onPromoChange() {
		if (promotion != null) {
			stagiaires = proxyPromotionBusiness.getStagiairesByPromo(promotion);			
		} else {
			stagiaires = new ArrayList<>();
		}
		System.out.println("NB de STAG = " + stagiaires.size());
	}

	public String afficherFicheStagiaire(Stagiaire stg) {

		
		String lien = "/FicheStagiaire.xhtml?id=" + stg.getId() + "&faces-redirect=true";
		return lien;
	}

	public String getNomRecherche() {
		return nomRecherche;
	}



	public void setNomRecherche(String nomRecherche) {
		this.nomRecherche = nomRecherche;
	}



	public String getMessageRecherche() {
		return messageRecherche;
	}



	public void setMessageRecherche(String messageRecherche) {
		this.messageRecherche = messageRecherche;
	}



	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}



	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}



	public Stagiaire getStagiaire() {
		return stagiaire;
	}



	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}



	public List<Promotion> getListPromotions() {
		return listPromotions;
	}



	public void setListPromotions(List<Promotion> listPromotions) {
		this.listPromotions = listPromotions;
	}



	public Promotion getPromotion() {
		return promotion;
	}



	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}



	public Titre getTitre() {
		return titre;
	}



	public void setTitre(Titre titre) {
		this.titre = titre;
	}



	public Cursus getCursusVise() {
		return cursusVise;
	}



	public void setCursusVise(Cursus cursusVise) {
		this.cursusVise = cursusVise;
	}



	public Provenance getProvenance() {
		return provenance;
	}



	public void setProvenance(Provenance provenance) {
		this.provenance = provenance;
	}



	public Diplome getDiplome() {
		return diplome;
	}



	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}



	public Dispositif getDispositif() {
		return dispositif;
	}



	public void setDispositif(Dispositif dispositif) {
		this.dispositif = dispositif;
	}



	public Test getTest() {
		return test;
	}



	public void setTest(Test test) {
		this.test = test;
	}



	public Financement getFinancement() {
		return financement;
	}



	public void setFinancement(Financement financement) {
		this.financement = financement;
	}



	public Entreprise getEntreprise() {
		return entreprise;
	}



	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}



	public StatutProfessionnel getStatutpro() {
		return statutpro;
	}



	public void setStatutpro(StatutProfessionnel statutpro) {
		this.statutpro = statutpro;
	}



	public Indemnisation getIndemnisation() {
		return indemnisation;
	}



	public void setIndemnisation(Indemnisation indemnisation) {
		this.indemnisation = indemnisation;
	}




}
