package fr.afcepf.ai106.projet3.web.controler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.ibusiness.PromotionIBusiness;

@ManagedBean(name = "mbFichePromo")
@ViewScoped
public class FichePromoManagedBean {

	private static final long serialVersionUID = 1L;
	private List<Cours> courses;
	private List<CursusMatiere> matieres;
	
	@ManagedProperty (value = "#{mbPromo.promo}")
	private Promotion promo;
	private String msg;
	private Date endDate;
	
	@EJB
	private PromotionIBusiness proxyPromoBu;
	

	public Date promoEndDate() {		
		return proxyPromoBu.endDate(promo);
	}
	
	@PostConstruct
	public void Init() {
		Map<String, String> params =FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer id=0;
		if(params.get("id") != null) {
		id = Integer.parseInt(params.get("id"));
		}
		if(id!=0) {
		promo = proxyPromoBu.getPromoById(id);
		}
		courses = proxyPromoBu.tousLesCours(promo);
		matieres = proxyPromoBu.getMatieres(promo);
		msg = "Promo " + promo.getNom() + " ajoutée. Commence le : " 
				+ promo.getDateDebut() + " pour une durée de : "
				+ proxyPromoBu.getDureeCursus(promo.getCursus()) + " demi-journées";
	}
	

	public List<Cours> getCourses() {
		return courses;
	}

	public void setCourses(List<Cours> courses) {
		this.courses = courses;
	}

	public Promotion getPromo() {
		return promo;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<CursusMatiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(List<CursusMatiere> matieres) {
		this.matieres = matieres;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	
	
		
}
