package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;

public interface StagiairePromoIDao extends GenericIDao<StagiairePromo>{

	Dispositif getDispositifByStagiaire(Stagiaire s);
	
	Promotion getPromotionByStagiaire(Stagiaire s);
	
	StagiairePromo enregisterUnStagiairePromo(Dispositif dispositif, Promotion promotion, Stagiaire stagiaire);
	
}
