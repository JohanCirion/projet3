package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Absence;

public interface AbsenceIDao extends GenericIDao<Absence> {
}
