package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Provenance;

public interface ProvenanceIDao extends GenericIDao<Provenance>{

}
