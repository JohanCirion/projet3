package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.Stagiaire;

public interface StagiaireIDao extends GenericIDao<Stagiaire> {

	public List<Stagiaire> getAllStagiaires();
	
	public List<Stagiaire> rechercherUnStagiaire(String string);
	
	public List<Stagiaire> getAllCandidats();
	
	public Stagiaire getStagiaireById(int id);
	
	public Provenance demoProvenance();
}
