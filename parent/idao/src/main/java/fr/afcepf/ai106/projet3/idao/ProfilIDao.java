package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Profil;

public interface ProfilIDao extends GenericIDao<Profil> {

}
