package fr.afcepf.ai106.projet3.idao;

import java.util.Date;
import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;

public interface PromotionIDao extends GenericIDao<Promotion> {
	
	List<Promotion> getAllPromotions();	
	List<Stagiaire> getStagiairesbyPromo(Promotion p);
	List<Cours> getAllCours(Promotion p);
	Integer getNumPromo(Cursus c);
	Date endDate(Promotion p);
	
}
