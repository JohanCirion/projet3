package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Test;

public interface TestIDao extends GenericIDao<Test> {
	
	Test getTestByStagiaire(Stagiaire s);

}
