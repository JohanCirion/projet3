package fr.afcepf.ai106.projet3.idao;


import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;

public interface StagiaireTestIDao extends GenericIDao<StagiaireTest> {

	StagiaireTest getStagiaireTestParStagiaire(Stagiaire s);
}
