package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Entreprise;

public interface EntrepriseIDao extends GenericIDao<Entreprise>{

}
