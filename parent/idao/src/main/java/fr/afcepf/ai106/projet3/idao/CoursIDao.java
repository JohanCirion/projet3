package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;


public interface CoursIDao extends GenericIDao<Cours>{

	List<Cours> getCoursByPromoMatiere(Promotion p, Matiere m);
}
