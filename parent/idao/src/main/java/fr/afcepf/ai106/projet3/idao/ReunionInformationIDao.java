package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Stagiaire;

public interface ReunionInformationIDao extends GenericIDao<ReunionInformation> {
	
	void inscrireStagiaireRI(ReunionInformation ri, Stagiaire s);

}
