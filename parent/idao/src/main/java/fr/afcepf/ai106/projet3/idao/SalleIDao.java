package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Salle;

public interface SalleIDao extends GenericIDao<Salle> {

}
