package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Entretien;
import fr.afcepf.ai106.projet3.entities.Stagiaire;

public interface EntretienIDao extends GenericIDao<Entretien> {
	
	public Entretien rechercherEntretienParStagiaire(Stagiaire s);

}
