package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;

public interface CursusMatiereIDao extends GenericIDao<CursusMatiere> {

	CursusMatiere rechercherUnCursusMatiere(Cursus c, Matiere m);
	List<CursusMatiere> rechercherParCursus(Cursus c);
	CursusMatiere rechercherParOrdre(Cursus c, Integer ordre);
	void inverserOrdre(CursusMatiere cm1, CursusMatiere cm2);
}
