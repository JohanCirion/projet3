package fr.afcepf.ai106.projet3.idao;

import java.util.List;

public interface GenericIDao<T> {

    
    T ajouter(T t);
    boolean supprimer(T t);
    T modifier(T t);
    T rechercher(Integer i);
    List<T> getAllGeneric();

}
