package fr.afcepf.ai106.projet3.idao;



import java.util.List;

import fr.afcepf.ai106.projet3.entities.Intervenant;


public interface IntervenantIDao extends GenericIDao<Intervenant> {

	Intervenant ajouter(Intervenant i);
	Intervenant modifier(Intervenant i);
	List<Intervenant> getAll();
	List<Intervenant> rechercherUnIntervenant(String string);
			
}
