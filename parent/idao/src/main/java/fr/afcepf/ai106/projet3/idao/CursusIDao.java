package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;

public interface CursusIDao extends GenericIDao<Cursus> {

	List<Cursus> getAll();
	List<CursusMatiere> getMatieres(Cursus c);
	int getDureeCursus(Cursus c);
	List<Cursus> getCursusValides();
	List<Cursus> getInvalidCursi();
}
