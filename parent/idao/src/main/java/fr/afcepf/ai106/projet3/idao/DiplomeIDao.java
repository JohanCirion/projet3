package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Diplome;

public interface DiplomeIDao extends GenericIDao<Diplome>{

}
