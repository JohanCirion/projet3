package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Matiere;

public interface MatiereIDao extends GenericIDao<Matiere> {

	public List<Matiere> getAll();
}
