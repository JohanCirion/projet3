package fr.afcepf.ai106.projet3.idao;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Administratif;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;

public interface UtilisateurIDao extends GenericIDao<Utilisateur> {
	
	Utilisateur Authenticate(String login, String password);
	
	Stagiaire getStagiaireByUtilisateur(Utilisateur u);
	
	List<Stagiaire> getStagiairebyName(String name);
	
	List<Intervenant> getIntervenantbyName(String name);
	
	List<Administratif> getAdministratifbyName(String name);
	
	Administratif getAdminByUtilisateur(Utilisateur u);
	
	Intervenant getIntervByUtilisateur(Utilisateur u);

}
