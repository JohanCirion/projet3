package fr.afcepf.ai106.projet3.idao;

import fr.afcepf.ai106.projet3.entities.Financement;

public interface FinancementIDao extends GenericIDao<Financement>{

}
