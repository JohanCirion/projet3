package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Promotion;


public interface IntervenantIBusiness {

	Intervenant ajouter(Intervenant i);
	Intervenant modifier(Intervenant i);
	List<Intervenant> getAll();
	Intervenant rechercher(Integer i);
	Cours affecterIntervenantCours(Cours c, Intervenant i);
	List<Cours> getAllCours();
	List<Intervenant> rechercherUnIntervenant(String string);
	Intervenant getIntervenantById(Integer id);
	
}
