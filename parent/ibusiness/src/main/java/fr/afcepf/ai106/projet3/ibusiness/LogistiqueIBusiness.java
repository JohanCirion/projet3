package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Salle;

public interface LogistiqueIBusiness {

	List<Salle> getAllSalle();
}
