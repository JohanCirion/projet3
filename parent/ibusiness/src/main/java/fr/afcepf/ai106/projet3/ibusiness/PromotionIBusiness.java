package fr.afcepf.ai106.projet3.ibusiness;

import java.util.Date;
import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Promotion;
import fr.afcepf.ai106.projet3.entities.Stagiaire;

public interface PromotionIBusiness {

	List<Promotion> getAllFuturesPromotions();
	
	List<Promotion> getAllPromotions();	
	
	Promotion getPromoById(Integer id);
	
	List<Cours> tousLesCours(Promotion p);
	
	List<Cours> getAllCours();
	
	List<CursusMatiere> getMatieres(Promotion p);
	
	List<Stagiaire> getStagiairesByPromo(Promotion p);
	
	Promotion getPromotionByStagiaire(Stagiaire s);
	
	Promotion ajouter(Promotion p);
	
	int getDureeCursus(Cursus c);
	
	List<Cours> getCoursMatiere(CursusMatiere cm);
	
	List<CursusMatiere> getAllCursusMatiere();
	
	Date calculDateFinCours (Cours c);
	
	Cours updateCours(Cours c);
	
	Cours getById(int id);
	
	Integer calculNumPromo(Cursus c);
	
	Date endDate(Promotion p);

}
