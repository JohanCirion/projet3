package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Administratif;
import fr.afcepf.ai106.projet3.entities.Intervenant;
import fr.afcepf.ai106.projet3.entities.Profil;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.Utilisateur;

public interface UtilisateurIBusiness {

	Utilisateur Authenticate(String login, String passwprd);
	
	Stagiaire getStagiaireByUtilisateur(Utilisateur u);

	void createLoginAndPasswordforStagiaire(Utilisateur u, Stagiaire s);
	
	List<Stagiaire> getStagiaireByName(String name);
	
	List<Intervenant> getIntervenantByName(String name);
	
	List<Administratif> getAdministratifByName(String name);
	
	List<Profil> getAllProfil();
	
	Intervenant getIntervenantByUtilisateur(Utilisateur u);
	
	Administratif getAdministratifByUtilisateur(Utilisateur u);
	
}
