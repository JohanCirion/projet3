package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Matiere;

public interface MatiereIBusiness {

	Matiere ajouter(Matiere m);
	List<Matiere> getAll();
	Matiere rechercher(Integer i);
	Matiere modifier(Matiere m);
}
