package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.CursusMatiere;
import fr.afcepf.ai106.projet3.entities.Matiere;

public interface CursusIBusiness {

	List<Cursus> getAll();
	void supprimerMatiereDUnCursus(Cursus c, Matiere m);
	void ajouterMatiereAUnCursus(Cursus c, Matiere m, Integer duree, Integer ordre);
	Cursus AjouterCursus(Cursus c);
	Cursus rechercher(Integer id);
	List<CursusMatiere> CursusMatiereDUnCursus(Cursus c);
	void monterDansUnCursus(CursusMatiere cm);
	void descendreDansUnCursus(CursusMatiere cm);
	void validerUnCursus(Cursus c);
	void invaliderUnCursus(Cursus c);
	List<Cursus> tousLesCursusValides();
	List<Cursus> getInvalidCursi();
}
