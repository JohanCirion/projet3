package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Cours;
import fr.afcepf.ai106.projet3.entities.Matiere;
import fr.afcepf.ai106.projet3.entities.Promotion;

public interface CoursIBusiness {

	List<Cours> getCoursByPromoMatiere(Promotion p, Matiere m);
}
