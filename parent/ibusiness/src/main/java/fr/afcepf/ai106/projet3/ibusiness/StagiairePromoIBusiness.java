package fr.afcepf.ai106.projet3.ibusiness;

import fr.afcepf.ai106.projet3.entities.StagiairePromo;

public interface StagiairePromoIBusiness {

	public StagiairePromo ajouterStagPromo(StagiairePromo sp);
}
