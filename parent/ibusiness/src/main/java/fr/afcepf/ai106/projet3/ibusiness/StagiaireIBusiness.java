package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.Titre;
import fr.afcepf.ai106.projet3.entities.Cursus;
import fr.afcepf.ai106.projet3.entities.Diplome;
import fr.afcepf.ai106.projet3.entities.Dispositif;
import fr.afcepf.ai106.projet3.entities.Entreprise;
import fr.afcepf.ai106.projet3.entities.Entretien;
import fr.afcepf.ai106.projet3.entities.Financement;
import fr.afcepf.ai106.projet3.entities.Provenance;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiairePromo;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;
import fr.afcepf.ai106.projet3.entities.StatutProfessionnel;
import fr.afcepf.ai106.projet3.entities.Indemnisation;
import fr.afcepf.ai106.projet3.entities.Promotion;

public interface StagiaireIBusiness {
	
	public Stagiaire enregistrerUnStagiaire(Stagiaire s);
	
	public Stagiaire modifierUnStagiaire(Stagiaire s);
	
	public List<Stagiaire> getAllStagiaires();
	
	public List<Stagiaire> rechercherUnStagiaire(String string);

	public Stagiaire rechercher(int id);
	
	public List<Titre> getAllTitres();
	
	public List<Cursus> getAllCursus();
	
	public List<Provenance> getAllProvenances();
	
	public List<Diplome> getAllDiplomes();
	
	public List<Dispositif> getAllDispositifs();
	
	public List<Financement> getAllFinancements();
	
	public List<Entreprise> getAllEntreprises();
	
	public List<StatutProfessionnel> getAllStatutPro();
	
	public List<Indemnisation> getAllIndemnisations();
	
	public List<StagiairePromo> getAllStagPromos();
	
	public Dispositif getDispositifByStagiaire(Stagiaire s);
	
	public StagiairePromo enregisterUnDispositif(StagiairePromo sp);
	
	public StagiairePromo enregisterUnStagiairePromo(Dispositif dispositif, Promotion promotion, Stagiaire stagiaire);
	
	public List<Entretien> getAllEntretien();
	
	public Entretien getEntretienParStagiaire(Stagiaire s);
	
	public StagiaireTest EnregistrerUnStagiaireTest(StagiaireTest st);
	
	public void enregistrerUnStagiaireAUnEntretien(Stagiaire s, Entretien e);
	
	public List<Stagiaire> getAllCandidats();
	
	public Provenance demoProvenance();
}
