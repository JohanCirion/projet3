package fr.afcepf.ai106.projet3.ibusiness;

import java.util.List;

import fr.afcepf.ai106.projet3.entities.ReunionInformation;
import fr.afcepf.ai106.projet3.entities.Stagiaire;
import fr.afcepf.ai106.projet3.entities.StagiaireTest;
import fr.afcepf.ai106.projet3.entities.Test;

public interface EvenementIBusiness {
	
	
	
	ReunionInformation AjouterRI(ReunionInformation ri);
	Test AjouterTest(Test t);
	List<Test> getAllTests();
	Test getTestByStagiaire(Stagiaire s);
	List<ReunionInformation> getAllRIFutur();
	public ReunionInformation rechercherRIParId(Integer id);
	public ReunionInformation modifierRI(ReunionInformation ri);
	List<ReunionInformation> getAllRI();
	void inscrireStagiaireAUneRi(ReunionInformation ri, Stagiaire s);
	StagiaireTest getStagiaireTestParStagiaire(Stagiaire t);
	
}
